import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { InvoiceCounter } from '../../../home/transaction/fin-01-invoice/model/invoice-counter';
import { InvoiceCounterRegion } from "../../shared/model/invoice-counter-region";
import { AppSharedService } from '../../shared/app-shared.service';
import { DashboardProgressService } from "../dashboard-progress.service";
import { InvoiceGeneration } from "../model/invoice-generation";
import * as dateFormat from 'dateformat';
import 'chartjs-plugin-datalabels';
import Swal from "sweetalert2";
@Component({
  selector: 'invoice-generation',
  templateUrl: './invoice-generation.component.html'
})
export class InvoiceGenerationComponent implements OnInit {

  constructor(private appSharedService: AppSharedService, public dashboardProgressService: DashboardProgressService) { }

  public changeFin01: string = "0";

  public selectedStateId: string = "0";
  public selectedDistrictId: string = "0";
  public stateFilterDatas: Array<any> = [];
  public districtFilterDatas: Array<any>;

  public invoiceGenerationFromDb: Array<InvoiceGeneration>;
  public invoiceGenerationDisplay: Array<InvoiceGeneration>;
  public fin01InvoiceCounter: InvoiceCounter = new InvoiceCounter();
  public fin02aInvoiceCounter: InvoiceCounter = new InvoiceCounter();
  public fin02InvoiceCounter: InvoiceCounter = new InvoiceCounter();
  public fin02bInvoiceCounter: InvoiceCounter = new InvoiceCounter();
  public invoiceCounterRegion: InvoiceCounterRegion;
  public chartDatas: Array<number>;
  public chartBackgroundColor: Array<string>;
  public chartLabels: any = [];
  public pieChartData: ChartDataSets[];
  public pieChartType = 'pie';
  public pieChartOptions: ChartOptions;
  public pieChartPlugin: any;
  private stateBgColorArray: Array<string>;
  public showChart: boolean = false;
  public currentDocName: string = "";
  public heading: string;
  public fin06barChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin01barChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public FIN01InvbarChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin07barChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin03abarChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public FIN02aInvbarChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin08bbarChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin08cbarChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin08barChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin03barChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public FIN02InvbarChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public FIN02bInvbarChartDatas: ChartDataSets[] = new Array<ChartDataSets>();
  public fin06barChartOptions: ChartOptions;
  public fin01barChartOptions: ChartOptions;
  public FIN01barChartOptions: ChartOptions;
  public fin07barChartOptions: ChartOptions;
  public fin03abarChartOptions: ChartOptions;
  public FIN02abarChartOptions: ChartOptions;

  public fin08bbarChartOptions: ChartOptions;
  public fin08cbarChartOptions: ChartOptions;
  public fin08barChartOptions: ChartOptions;
  public fin03barChartOptions: ChartOptions;
  public FIN02barChartOptions: ChartOptions;
  public FIN02bbarChartOptions: ChartOptions;
  // private barChartColors: Array<string> = ['#FA4848','#FFA500','#90EE90'];
  private barChartColors: Array<string> = ['#f44336', '#ffb822', '#0abb87'];
  public fin01InvDisplay: Boolean = false;
  public fin02aInvDisplay: Boolean = false;
  public fin02InvDisplay: Boolean = false;
  public fin02bInvDisplay: Boolean = false;
  public loading: Boolean = false;
  public loadingFin01: Boolean = false;
  public loadingFin02a: Boolean = false;
  public loadingFin02: Boolean = false;
  public loadingFin02b: Boolean = false;
  public barChartLabels = ['Initiate', 'In Progress', 'Completed'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public currentState: string = "";
  public stateName: string = "";
  public circleName: string = "";
  public districtName: string = "";
  public currentDate: string;
  public totalClinic: number;
  public totalEquipment: number;
  public totalAmount: number;
  public disableState: boolean = false;
  public selectedDate: any;
  public prevMonth: any;
  public year: any;
  public barChartClickEvent: any;
  public disableButton: boolean = false;

  public disableFin02B: boolean = false;
  public disableFin02A: boolean = false;
  public disableFin02: boolean = false;
  public disableFin01: boolean = false;

  @ViewChild('fin01Case') fin01Case: ElementRef;
  @ViewChild('fin02Case') fin02Case: ElementRef;
  @ViewChild('fin02ACase') fin02ACase: ElementRef;
  @ViewChild('fin02BCase') fin02BCase: ElementRef;

  ngOnInit() {

    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let monthStr;
    let dateStr;

    if (month < 10) { monthStr = "0" + month }
    else { monthStr = month }
    (day < 10 ? dateStr = "0" + day : dateStr = day);
    this.selectedDate = dateStr + "-" + monthStr + "-" + year

    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];

    let d = new Date();
    this.prevMonth = monthNames[d.getMonth() - 1];
    if (this.prevMonth == 'January') {

      this.year = d.getFullYear() - 1
    }
    else {
      this.year = d.getFullYear()
    }



    this.invoiceGenerationFromDb = [];
    this.invoiceGenerationDisplay = [];
    this.appSharedService.fetchAllStates().subscribe(x => {
      this.stateFilterDatas = x;
    })
    this.currentDate = dateFormat(new Date(), 'dd/mm/yyyy');
    this.invoiceCounterRegion = new InvoiceCounterRegion();
    let region: any[][];
    this.loading = true;
    this.loadingFin01 = true;
    this.loadingFin02a = true;
    this.loadingFin02 = true;
    this.loadingFin02b = true;
    let bool1 = null
    let bool2 = null
    let bool3 = null
    let bool4 = null
    // this.appSharedService.getRegionBasedOnUser().then(
    //     (val:any[][]) => {;region=val;            
    //         this.invoiceCounterRegion.states= (region[0].length==0?"0":region[0].toString());
    //         this.invoiceCounterRegion.circles= (region[1].length==0?"0":region[1].toString());
    //         this.invoiceCounterRegion.districts= (region[2].length==0?"0":region[2].toString());
    //         this.invoiceCounterRegion.clinics= (region[3].length==0?"0":region[3].toString());
    this.dashboardProgressService.getFin02aInvoiceCounter().subscribe(x => {
      this.fin02aInvoiceCounter = x;
      this.loading = true;
      this.assignBarChart("fin02a");
      setTimeout(() => {
        bool1 = true
        if (bool1 == true && bool2 == true && bool3 == true && bool4 == true) {
          this.loading = false
        }
      }, 100);

    });
    this.dashboardProgressService.getFin01InvoiceCounter().subscribe(x => {
      this.fin01InvoiceCounter = x;
      this.loading = true;
      this.assignBarChart("fin01");
      setTimeout(() => {
        bool2 = true
        if (bool1 == true && bool2 == true && bool3 == true && bool4 == true) {
          this.loading = false
        }
      }, 100);
    });


    this.dashboardProgressService.getFin02InvoiceCounter().subscribe(x => {
      this.fin02InvoiceCounter = x;
      // console.log(x)
      this.loading = true;
      this.assignBarChart("fin02");
      setTimeout(() => {
        bool3 = true
        if (bool1 == true && bool2 == true && bool3 == true && bool4 == true) {
          this.loading = false
        }
      }, 100);
    });
    this.dashboardProgressService.getFin02bInvoiceCounter().subscribe(x => {
      this.fin02bInvoiceCounter = x;
      this.loading = true;
      this.assignBarChart("fin02b");
      setTimeout(() => {
        bool4 = true
        if (bool1 == true && bool2 == true && bool3 == true && bool4 == true) {
          this.loading = false
        }
      }, 100);
    });
    //   },
    //   (err) => console.error(err)
    // );

    this.showChart = false;
    this.chartDatas = new Array<number>();
    this.chartLabels = new Array();
    this.chartBackgroundColor = new Array<string>();
    this.chartLabels = new Array<string>();
    this.pieChartData = new Array<ChartDataSets>();

    this.stateBgColorArray = ['#3FC380', '#89C4F4', '#E08283', '#B388DD', '#F2C300', '#41C3AC', '#81B9C3', '#FF884D', '#FF9124', '#069BFF', '#FF6B57', '#3FC380', '#89C4F4', '#E08283', '#B388DD', '#F2C300', '#41C3AC', '#81B9C3', '#FF884D', '#FF9124', '#069BFF', '#FF6B57', '#F2C300', '#41C3AC'];
    this.chartBackgroundColor = this.stateBgColorArray;
  }

  handleForm(event) {
    switch (event.target.id) {
      case "state":
        this.selectedStateId = event.target.value;
        this.selectedDistrictId = "0";
        this.districtFilterDatas = this.stateFilterDatas.find(state => state.id == event.target.value).districts;
        //this.filterFunction();
        break;
      case "district":
        this.selectedDistrictId = event.target.value;
        break;
      case "fin01Case":
        this.disableFin02 = true;
        this.disableFin02A = true;
        this.disableFin02B = true;
        break;
      default:
        break;
    }
  }
  clickFn(event) {
    if (event.active[0] != undefined) {
      this.forwardFlow(event);
    }
  }

  forwardFlow(event) {

    if (this.currentState == 'state') {
      this.heading = this.currentDocName + " - Circlewise Clinic Splitup "
      this.loading = true;
      this.stateName = (event != undefined ? this.chartLabels[event.active[0]._index] : this.stateName);
      this.resetArray();
      this.circleWisePieChart();
      this.currentState = 'circle';
      this.assignChartDatas();
    } else if (this.currentState == 'circle') {
      this.heading = this.currentDocName + " - Districtwise Clinic Splitup "
      this.loading = true;
      this.circleName = (event != undefined ? this.chartLabels[event.active[0]._index] : this.circleName);
      this.resetArray();
      this.invoiceGenerationFromDb.forEach((y => {
        if (y.circleName == this.circleName) {
          let invGen = new InvoiceGeneration();
          this.totalClinic = y.clinicCount + this.totalClinic;
          y.amount = Math.trunc(y.amount);
          this.totalEquipment = y.equipmentCount + this.totalEquipment;
          this.totalAmount = y.amount + this.totalAmount;
          let uniqueInvGen = this.invoiceGenerationDisplay.filter((inv => inv.districtId === y.districtId))[0];
          if (uniqueInvGen != undefined && uniqueInvGen != null) {
            invGen.stateId = uniqueInvGen.stateId;
            invGen.stateName = uniqueInvGen.stateName;
            invGen.circleName = undefined;
            invGen.districtId = uniqueInvGen.districtId;
            invGen.districtName = uniqueInvGen.districtName;
            invGen.amount = y.amount + uniqueInvGen.amount;
            invGen.equipmentCount = y.equipmentCount + uniqueInvGen.equipmentCount;
            invGen.clinicCount = y.clinicCount + uniqueInvGen.clinicCount;
            var index = this.invoiceGenerationDisplay.findIndex(item => item.districtId == y.districtId);
            this.invoiceGenerationDisplay.splice(index, 1, invGen)
          } else {
            this.invoiceGenerationDisplay.push(y);
          }
        }

      }));
      this.invoiceGenerationDisplay.forEach((x) => {
        this.chartDatas.push(x.clinicCount);
        this.chartLabels.push(x.districtName);
      })

      this.currentState = 'district';
      this.assignChartDatas();
    }
  }

  backButton() {
    this.resetArray();
    if (this.currentState == 'district') {
      this.heading = this.currentDocName + " - Circlewise Clinic Splitup ";
      this.circleWisePieChart();
      this.districtName = "";
      this.circleName = "";
      this.currentState = 'circle';
      this.assignChartDatas();
    } else if (this.currentState == 'circle') {
      this.heading = this.currentDocName + " - Statewise Clinic Splitup"
      this.stateWisePieChart();
      this.stateName = "";
      this.currentState = 'state';
      this.assignChartDatas();
    }
    else if (this.currentState == 'state') {
      this.disableState = false;
      this.showChart = false;
      this.disableButton = false;
      this.disableFin02B = false;
      this.disableFin02A = false;
      this.disableFin02 = false;
      this.disableFin01 = false;
      // console.log(this.disableButton,this.disableState)
    }
  }

  generateChart(invoiceGenerationFromDb: Array<InvoiceGeneration>) {
    // console.log('x')
    if (invoiceGenerationFromDb.length > 0) {

      this.invoiceGenerationFromDb = invoiceGenerationFromDb;
      this.stateWisePieChart();
      this.currentState = 'state';
      this.assignChartDatas();
    } else {

      this.disableState = false;
      this.showChart = false;
      this.disableButton = false;
      this.loading = false;
      this.loadingFin01 = false;
      this.loadingFin02a = false;
      this.loadingFin02 = false;
      this.loadingFin02b = false;
      console.log(this.disableButton,this.disableState)

      Swal.fire('', 'No data available', 'info');
      // console.log(swal)
    }

  }

  go() {
    // console.log(event)
    // let fin01= document.getElementById('fin01Case');

    // switch (this.change[]) {

    //       case "fin01Case":
    //         // this.changeFin01 = event.target.value;
    //       this.disableFin02 = true;
    //       this.disableFin02A = true;
    //       this.disableFin02B = true;
    //       console.log('test')
    //       break;
    //   default:
    //     break;
    // }

    // console.log('y')
    if (this.fin01Case) {

      this.disableFin02 = true;
      this.disableFin02A = true;
      this.disableFin02B = true;
    }
    if (this.fin02Case) {
      this.disableFin01 = true;
      this.disableFin02A = true;
      this.disableFin02B = true;
    }
    if (this.fin02ACase) {
      this.disableFin02 = true;
      this.disableFin01 = true;
      this.disableFin02B = true;
    }
    if (this.fin02BCase) {
      this.disableFin02 = true;
      this.disableFin02A = true;
      this.disableFin01 = true;
    }
    var d = new Date();
    if (this.barChartClickEvent != undefined) {
      // if(this.barChartClickEvent.active[0].length ==0){
      //   Swal.fire('', 'No data available', 'info');
      //   console.log('s',this.barChartClickEvent.length)
      // }
      this.disableState = true;
      this.disableButton = true;

      // console.log(this.disableButton,this.disableState)

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 06 DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 06 DOC - Initiate";
        // console.log(this.barChartClickEvent.active[0]._chart.config.options.title.text)
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin06Intitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
          // console.log(x)
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 06 DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 06 DOC - Inprogress";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin06Inprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
          // console.log(x)

        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 06 DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 06 DOC - Completed";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin06Approved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
          // console.log(x)

        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 01 DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 01 DOC - Initiate";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin01Intitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 01 DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 01 DOC - Inprogress";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin01Inprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 01 DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 01 DOC - Completed";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin01Approved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 01 INVOICE' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "FIN 01 Invoice - Initiate";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin01InvIntitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 01 INVOICE' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "FIN 01 Invoice - Inprogress";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin01InvInprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 01 INVOICE' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "FIN 01 Invoice - Completed";
        this.loading = true;
        this.loadingFin01 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin01InvApproved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }


      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 07 DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 07 DOC - Initiate";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin07Intitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 07 DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 07 DOC - Inprogress";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin07Inprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 07 DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 07 DOC - Completed";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin07Approved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 03a DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 03a DOC - Initiate";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin03aIntitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 03a DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 03a DOC - Inprogress";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin03aInprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 03a DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 03a DOC - Completed";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin03aApproved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02a INVOICE' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "FIN 02a Invoice - Initiate";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02aInvIntitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02a INVOICE' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "FIN 02a Invoice - Inprogress";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02aInvInprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02a INVOICE' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "FIN 02a Invoice - Completed";
        this.loading = true;
        this.loadingFin02a = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02aInvApproved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }


      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08b DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 08b DOC - Initiate";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08bIntitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08b DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 08b DOC - Inprogress";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08bInprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08b DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 08b DOC - Completed";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08bApproved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08c DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 08c DOC - Initiate";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08cIntitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08c DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 08c DOC - Inprogress";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08cInprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08c DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 08c DOC - Completed";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08cApproved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08 DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 08 DOC - Initiate";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08Intitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08 DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 08 DOC - Inprogress";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08Inprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 08 DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 08 DOC - Completed";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08Approved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 03 DOC' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 03 DOC - Initiate";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin03Intitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 03 DOC' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 03 DOC - Inprogress";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin03Inprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'Fin 03 DOC' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 03 DOC - Completed";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin08bApproved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02 INVOICE' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "Fin 02 Invoice - Initiate";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02Intitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
          // console.log(x)

        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02 INVOICE' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "Fin 02 Invoice - Inprogress";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02Inprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
          // console.log(x)
        })
      }
      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02 INVOICE' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "Fin 02 Invoice - Completed";
        this.loading = true;
        this.loadingFin02 = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02Approved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
          // console.log(x)

        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02b INVOICE' && this.barChartClickEvent.active[0]._index == 0) {
        this.currentDocName = "FIN 02B Invoice - Initiate";
        this.loading = true;
        this.loadingFin02b = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02bInvIntitate(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02b INVOICE' && this.barChartClickEvent.active[0]._index == 1) {
        this.currentDocName = "FIN 02B Invoice - Inprogress";
        this.loading = true;
        this.loadingFin02b = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02bInvInprogress(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      if (this.barChartClickEvent.active[0]._chart.config.options.title.text == 'FIN 02b INVOICE' && this.barChartClickEvent.active[0]._index == 2) {
        this.currentDocName = "FIN 02B Invoice - Completed";
        this.loading = true;
        this.loadingFin02b = true;
        this.resetArray();
        this.dashboardProgressService.getInvoiceGenerationFin02bInvApproved(d.getMonth(), d.getFullYear(), this.selectedStateId, this.selectedDistrictId).subscribe((x) => {
          this.generateChart(x);
        })
      }

      this.heading = this.currentDocName + " - Statewise Clinic Splitup"
    } else {
      this.disableFin01 = false;
      this.disableFin02 = false;
      this.disableFin02A = false;
      this.disableFin02B = false;
      Swal.fire('', 'Please select an particular invoice in bar chart and then click Go button', 'info');
    }
  }
  clickBarChart(event) {
    this.barChartClickEvent = event;

  }

  stateWisePieChart() {

    this.invoiceGenerationFromDb.forEach((y => {
      let invGen = new InvoiceGeneration();
      this.totalClinic = y.clinicCount + this.totalClinic;
      this.totalEquipment = y.equipmentCount + this.totalEquipment;
      y.amount = Math.trunc(y.amount);
      this.totalAmount = y.amount + this.totalAmount;
      let uniqueInvGen = this.invoiceGenerationDisplay.filter((inv => inv.stateId === y.stateId))[0];
      if (uniqueInvGen != undefined && uniqueInvGen != null) {
        invGen.stateId = uniqueInvGen.stateId;
        invGen.stateName = uniqueInvGen.stateName;
        invGen.amount = y.amount + uniqueInvGen.amount;
        invGen.equipmentCount = y.equipmentCount + uniqueInvGen.equipmentCount;
        invGen.clinicCount = y.clinicCount + uniqueInvGen.clinicCount;
        var index = this.invoiceGenerationDisplay.findIndex(item => item.stateId == y.stateId);
        this.invoiceGenerationDisplay.splice(index, 1, invGen)
      } else {
        this.invoiceGenerationDisplay.push(y);
      }
    }));
    this.invoiceGenerationDisplay.forEach((x) => {
      this.chartDatas.push(x.clinicCount);
      this.chartLabels.push(x.stateName);
    })
  }
  circleWisePieChart() {
    this.invoiceGenerationFromDb.forEach((y => {
      if (y.stateName == this.stateName) {
        this.totalClinic = y.clinicCount + this.totalClinic;
        y.amount = Math.trunc(y.amount);
        this.totalEquipment = y.equipmentCount + this.totalEquipment;
        this.totalAmount = y.amount + this.totalAmount;
        let invGen = new InvoiceGeneration();
        let uniqueInvGen = this.invoiceGenerationDisplay.filter((inv => inv.circleId === y.circleId))[0];
        if (uniqueInvGen != undefined && uniqueInvGen != null) {
          invGen.stateId = uniqueInvGen.stateId;
          invGen.stateName = uniqueInvGen.stateName;
          invGen.circleId = uniqueInvGen.circleId;
          invGen.circleName = uniqueInvGen.circleName;
          invGen.districtName = undefined;
          invGen.amount = y.amount + uniqueInvGen.amount;
          invGen.equipmentCount = y.equipmentCount + uniqueInvGen.equipmentCount;
          invGen.clinicCount = y.clinicCount + uniqueInvGen.clinicCount;
          var index = this.invoiceGenerationDisplay.findIndex(item => item.circleId == y.circleId);
          this.invoiceGenerationDisplay.splice(index, 1, invGen)
        } else {
          this.invoiceGenerationDisplay.push(y);
        }
      }

    }));

    this.invoiceGenerationDisplay.forEach((x) => {
      this.chartDatas.push(x.clinicCount);
      this.chartLabels.push(x.circleName);
    });
  }
  assignChartDatas() {
    this.pieChartData.push({
      data: this.chartDatas,
      backgroundColor: this.chartBackgroundColor,
      label: 'Dataset 1',

    });
    this.pieChartOptions = {
      responsive: true,
      tooltips: { enabled: true },
      title: {
        display: true,
        fontSize: 14,
        fontColor: 'black'
      },
      legend: {
        display: true,
        position: 'left',

        labels: {
          fontSize: 14,
          fontColor: 'black'
        }
      },
      plugins: {
        datalabels: {
          color: 'black',


          font: {
            weight: 'bold'
          }

        },


      }
    };
    this.showChart = true;
    this.loading = false;
    this.loadingFin01 = false;
    this.loadingFin02a = false;
    this.loadingFin02 = false;
    this.loadingFin02b = false;

  }

  assignBarChart(invoice) {
    if (invoice == 'fin01') {


      // console.log('a')
      this.fin06barChartDatas.push(
        { data: [this.fin01InvoiceCounter.currMonSd1NotCreated, this.fin01InvoiceCounter.currMonSd1InProgress, this.fin01InvoiceCounter.currMonSd1Approved], backgroundColor: this.barChartColors, }

      );
      this.fin01barChartDatas.push(
        { data: [this.fin01InvoiceCounter.currMonSd2NotCreated, this.fin01InvoiceCounter.currMonSd2InProgress, this.fin01InvoiceCounter.currMonSd2Approved], backgroundColor: this.barChartColors, }
      );
      this.FIN01InvbarChartDatas.push(
        { data: [this.fin01InvoiceCounter.currMonInvNotCreated, this.fin01InvoiceCounter.currMonInvInProgress, this.fin01InvoiceCounter.currMonInvApproved], backgroundColor: this.barChartColors, }
      );

      this.fin06barChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 06 DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },


      };

      this.fin01barChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 01 DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },

      };
      this.FIN01barChartOptions = {
        responsive: true,
        title: { display: true, text: 'FIN 01 INVOICE', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },
      }
      this.fin01InvDisplay = true;
      this.loadingFin01 = false;
    }
    if (invoice == 'fin02a') {

      this.fin07barChartDatas.push(
        { data: [this.fin02aInvoiceCounter.currMonSd1NotCreated, this.fin02aInvoiceCounter.currMonSd1InProgress, this.fin02aInvoiceCounter.currMonSd1Approved], backgroundColor: this.barChartColors, }

      );
      this.fin03abarChartDatas.push(
        { data: [this.fin02aInvoiceCounter.currMonSd2NotCreated, this.fin02aInvoiceCounter.currMonSd2InProgress, this.fin02aInvoiceCounter.currMonSd2Approved], backgroundColor: this.barChartColors, }
      );
      this.FIN02aInvbarChartDatas.push(
        { data: [this.fin02aInvoiceCounter.currMonInvNotCreated, this.fin02aInvoiceCounter.currMonInvInProgress, this.fin02aInvoiceCounter.currMonInvApproved], backgroundColor: this.barChartColors, }
      );

      this.fin07barChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 07 DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },


      };

      this.fin03abarChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 03a DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },

      };
      this.FIN02abarChartOptions = {
        responsive: true,
        title: { display: true, text: 'FIN 02a INVOICE', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },
      }
      this.fin02aInvDisplay = true;
      this.loadingFin02a = false;
    }
    if (invoice == 'fin02') {

      // this.loading=true

      this.fin08bbarChartDatas.push(
        { data: [this.fin02InvoiceCounter.currMonSd1NotCreated, this.fin02InvoiceCounter.currMonSd1InProgress, this.fin02InvoiceCounter.currMonSd1Approved], backgroundColor: this.barChartColors, }

      );
      this.fin08cbarChartDatas.push(
        { data: [this.fin02InvoiceCounter.currMonSd2NotCreated, this.fin02InvoiceCounter.currMonSd2InProgress, this.fin02InvoiceCounter.currMonSd2Approved], backgroundColor: this.barChartColors, }
      );
      this.fin08barChartDatas.push(
        { data: [this.fin02InvoiceCounter.currMonSd3NotCreated, this.fin02InvoiceCounter.currMonSd3InProgress, this.fin02InvoiceCounter.currMonSd3Approved], backgroundColor: this.barChartColors, }
      );
      this.fin03barChartDatas.push(
        { data: [this.fin02InvoiceCounter.currMonSd4NotCreated, this.fin02InvoiceCounter.currMonSd4InProgress, this.fin02InvoiceCounter.currMonSd4Approved], backgroundColor: this.barChartColors, }
      );
      this.FIN02InvbarChartDatas.push(
        { data: [this.fin02InvoiceCounter.currMonInvNotCreated, this.fin02InvoiceCounter.currMonInvInProgress, this.fin02InvoiceCounter.currMonInvApproved], backgroundColor: this.barChartColors, }
      );

      this.fin08bbarChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 08b DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },


      };

      this.fin08cbarChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 08c DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },

      };

      this.fin08barChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 08 DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },

      };

      this.fin03barChartOptions = {
        responsive: true,
        title: { display: true, text: 'Fin 03 DOC', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },

      };
      this.FIN02barChartOptions = {
        responsive: true,
        title: { display: true, text: 'FIN 02 INVOICE', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },
      }
      this.fin02InvDisplay = true;
      this.loadingFin02 = false;
    }
    if (invoice == 'fin02b') {
      // this.loading=true


      this.FIN02bInvbarChartDatas.push(
        { data: [this.fin02bInvoiceCounter.currMonInvNotCreated, this.fin02bInvoiceCounter.currMonInvInProgress, this.fin02bInvoiceCounter.currMonInvApproved], backgroundColor: this.barChartColors, }
      );

      this.FIN02bbarChartOptions = {
        responsive: true,
        title: { display: true, text: 'FIN 02b INVOICE', fontSize: 14, fontColor: 'black', },
        legend: { display: false, },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'black'

            }
          }]
        },
      }
      this.fin02bInvDisplay = true;
      this.loadingFin02b = false;
    }

    this.loading=false;

  }

  resetArray() {
    this.totalClinic = 0;
    this.totalEquipment = 0;
    this.totalAmount = 0;
    this.chartLabels = [];
    this.chartDatas = [];
    this.pieChartData = [];
    this.invoiceGenerationDisplay = [];
  }
}
