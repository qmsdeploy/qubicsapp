import { Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { SelectModule } from 'ng-select';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { AppSharedModule } from './../../shared/app-shared.module';
import { Fin02bEquipmentRoutingModule } from './fin-02b-equipment-routing.module';
import { Fin02bCreateEquipmentComponent } from './fin-02b-create-equipment/fin-02b-create-equipment.component';
import { Fin02bEquipmentService } from './fin-02b-equipment-service';
import { Fin02bequipmentModel } from './model/cimsfino2b-equipment-model';

@NgModule({
  imports: [
    CommonModule,
    SelectModule,
    SharedModule,
    Fin02bEquipmentRoutingModule,
    DataTablesModule,
    NgbProgressbarModule,
    NgbTabsetModule,
    AppSharedModule
  ],
  declarations: [ Fin02bCreateEquipmentComponent ],
  providers: [ Fin02bEquipmentService ,Fin02bequipmentModel ]

})
export class Fin02bEquipmentModule { 

  constructor(){}

  ngOnInit(): void {
  }

  
}
