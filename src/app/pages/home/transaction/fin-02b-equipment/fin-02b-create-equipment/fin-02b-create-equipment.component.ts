import { element } from 'protractor';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import pdfMake from "pdfmake/build/pdfmake.min";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import * as dateFormat from 'dateformat';
import { Fin02bEquipmentService } from '../fin-02b-equipment-service';
import { Fin02bequipmentModel } from "../model/cimsfino2b-equipment-model";
import { DataTableDirective } from 'angular-datatables';
import { Subject, Subscription } from 'rxjs';
import { data } from 'jquery';
@Component({
    selector: 'fin-02b-create-equipment',
    templateUrl: './fin-02b-create-equipment.html',
})
export class Fin02bCreateEquipmentComponent implements OnInit {

  dtOptions: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength: 10
  };

  dtOptions1: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength: 10,
    lengthChange: false
  };
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
    
  constructor(private Datas : Fin02bequipmentModel , private route: ActivatedRoute, private fin02bequipmentservice : Fin02bEquipmentService, private modalService : NgbModal) { }

  public stateFilterDatas:any;
  public districtFilterDatas:any;
  public equipmentCount:any;
  public equipmentList:any;
  public tmp1: Subscription;
  public tmp2:any;
  public xFlag:any;
  public tmpData:any;
  public equipmentListType:any;
  public tmpequipmentList:any;
  public tmpState:any;
  public loading:boolean;

  ngOnInit(): void {
    this.fin02bequipmentservice.fetchStateDetails().subscribe(x => {
      this.stateFilterDatas = x;
    }) 
    this.xFlag = 0;
  }

  handleForm(event:any){
    switch(event.target.id){

      case 'state':
        this.Datas.state = event.target.value;
        this.tmpState = event.target.value;
        // console.log('State',this.Datas.state);
        this.districtFilterDatas = this.stateFilterDatas.find(state => state.stateName == event.target.value).districts;
        break;
      case 'district':
        this.Datas.district = event.target.value;
        // console.log('District',this.Datas.district);
        break;
      case 'clinicType':
        this.Datas.clinicType = event.target.value;
        // console.log('ClinicType',this.Datas.clinicType);
        break;
      case 'period':
        this.Datas.period = event.target.value;
        // console.log('Period',this.Datas.period);
        break;
      case 'year':
        this.Datas.year = event.target.value;
        // console.log('Year',this.Datas.year);
        break;
      default:
        break;

    }
    
  }

  filterFunction(){
    // console.log('workinh...');
    this.loading = true;
    this.Datas.datas=[];
    this.fin02bequipmentservice.fetchEquipmentCount().subscribe((x) => {    
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        
        this.equipmentCount = x;
        //stateFilter:
        if(this.Datas.state == 'ALL'){
          this.Datas.datas = this.equipmentCount;
        }
        else if(this.Datas.state != 'ALL' && this.Datas.state != null){
          this.Datas.datas = this.equipmentCount.filter(state => state.stateName === this.Datas.state)
        }
        else{
          //nothing
        }
        // districtFilter
        if(this.Datas.district == 'ALL'){
          // console.log(this.Datas.district);
          this.Datas.datas = this.Datas.datas;
        }
        else if(this.Datas.district != 'ALL' && this.Datas.district != null){
          this.Datas.datas = this.Datas.datas.filter(district => district.districtName === this.Datas.district)
        }
        else{
          //nothing
        }
        //cliniTypeCode filter
        if(this.Datas.clinicType == 'ALL' || this.Datas.clinicType == null){
          this.Datas.datas = this.Datas.datas;
        }
        else if(this.Datas.clinicType != 'ALL' && this.Datas.clinicType != null){
          this.Datas.datas = this.Datas.datas.filter(clinicTypeCode => clinicTypeCode.clinicTypeCode == this.Datas.clinicType)
        }
        else{
          //nothing
        }
        //period
        if(this.Datas.period == 'ALL' || this.Datas.period == null){
          this.Datas.datas = this.Datas.datas;
        }
        else if(this.Datas.period != 'ALL' && this.Datas.period != null){
          this.Datas.datas = this.Datas.datas.filter(period => period.period == this.Datas.period)
        }
        else{
          //nothing
        }
        //year
        if(this.Datas.year == 'ALL' || this.Datas.year == null){
          this.Datas.datas = this.Datas.datas;
        }
        else if(this.Datas.year != 'ALL' && this.Datas.year != null){
          this.Datas.datas = this.Datas.datas.filter(year => year.year == this.Datas.year)
        }
        else{
          //nothing
        }      
        this.equipmentCount = this.Datas.datas;
        // console.log('EquipmentCount:',this.equipmentCount);
        this.dtTrigger.next();
        this.loading = false;
      })
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
    // console.log('BByyyeeeeeeeeeeeee');
  }

  resetFilter(){
    this.dtOptions.destroy;
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
        });
        this.dtTrigger.next();
    this.equipmentCount=[];
    this.Datas.datas = [];
    this.Datas.dataClinicTypeCode = '';
    this.Datas.dataDistrictName = '';
    this.Datas.dataStateName = '';
    this.Datas.dataPeriod ='';
    this.Datas.dataYear = '';
    this.Datas.clinicType = '';
    this.Datas.district = '';
    this.Datas.state ='';
    this.Datas.period='';
    this.Datas.year = '';
  }
 


  openModal(template,data:any) {
    this.tmpData = data;
    if(this.xFlag != 0){
      this.tmp1.unsubscribe();
      this.xFlag += 1; 
    }
    // this.modalService.open(template);
    // console.log('data',data);
    this.equipmentList = [];
    this.tmp1 = this.fin02bequipmentservice.fetchEquipmentList(data.stateId,data.districtId,data.clinicTypeId,data.period,data.year).subscribe((x)=>{
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //dtInstance.destroy();
        // console.log('template',template._declarationTContainer.localNames[0]);
        // console.log('data',data);
        //
        this.Datas.dataStateName = data.stateName;
        this.Datas.dataDistrictName = data.districtName;
        this.Datas.dataClinicTypeCode = data.clinicTypeCode;
        this.Datas.dataPeriod = data.period;
        this.Datas.dataYear = data.year;
        //
        switch(template._declarationTContainer.localNames[0]){
          case 'template1' :
            //
            this.equipmentListType = 'Already Existing Equipment';
            for(let element of x){
              if(element.periodStatus == "EE"){
                this.equipmentList.push(element)
              }
            }
            break;
          case 'template2' :
            //
            this.equipmentListType = 'Added & Active Equipment';
            for(let element of x){
              if(element.periodStatus == "AA"){
                this.equipmentList.push(element)
              }
            }
            break;
          case 'template3' :
            //
            this.equipmentListType = 'Removed & Deactivated Equipment';
            for(let element of x){
              if(element.periodStatus == "RD"){
                this.equipmentList.push(element)
              }
            }
            break;
          case 'template4':
            //
            this.equipmentListType = '';
            for(let element of x){
              if(element.periodStatus == "EE"){
                element.cnd1 = 'Y'
                element.cnd2 = ''
                element.cnd3 = ''
                this.equipmentList.push(element)
              }
              if(element.periodStatus == "AA"){
                element.cnd1 = ''
                element.cnd2 = 'Y'
                element.cnd3 = ''
                this.equipmentList.push(element)
              }
              if(element.periodStatus == "RD"){
                element.cnd1 = ''
                element.cnd2 = ''
                element.cnd3 = 'Y'
                this.equipmentList.push(element)
              }
            }
            break;

        }
        
      });
      // console.log(this.equipmentList);
      this.modalService.open(template, {
        size: 'lg', centered: true,
      });
    });
    // console.log('function Ends');
  }

  // view(){

  // }  

  downloadPdf(){
    // console.log('PDF');
    let logo = this.Datas.logo;
    let equipListType = this.equipmentListType;
    let statePdf =this.Datas.dataStateName;
    let districtPdf = this.Datas.dataDistrictName;
    let clinicTypeCodePdf = this.Datas.dataClinicTypeCode;
    let periodPdf = this.Datas.dataPeriod;
    let yearPdf = this.Datas.dataYear;
    const document ={        
      pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
          columns:[
          {
            image: logo,
            fit: [275, 125],
            alignment: 'center',
            margin: [0, 0, 0, 10],
            
          },
        ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
        {
          text: '\nFIN-2B Equipment List -' + this.equipmentListType + '\n',
          style: 'header'
        },
  
        {
          text: this.Datas.dataStateName + ' State/ ' + this.Datas.dataDistrictName +' District/ ' + this.Datas.dataClinicTypeCode + ' / ' + this.Datas.dataPeriod + ' Period/ ' + this.Datas.dataYear + ' Year'+'\n',
          style: 'subHeader'
        },
  
        {
          style: 'table',
  
          table: {
            widths:[100,'*'],
            body: [
              [ {text : 'BE Number' , style : 'tableHead'},
               {text : 'BE Name' , style : 'tableHead'}],
              ...this.equipmentList.map(x =>([x.benumber,x.assetName])),
            ]
          },
          alignment : 'left',
        },
        
  
        ],

      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'center'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }

    }
    pdfMake.createPdf(document).download("FIN-2B-EquipmentList"+this.equipmentListType +".pdf");

  }

  generateExcel(){
  //
  // console.log('xcel start..');
  let workbook = new Workbook();
  let worksheet = workbook.addWorksheet('EquipmentList -'+this.equipmentListType);
  let data = [];
  //console.log(data);

  const title = worksheet.addRow(['', '', 'FIN-2B EquipmentList -'+ this.equipmentListType,'']);
  title.font={
    size: 12,
    bold:true
  }
  worksheet.mergeCells('D1:H1');
  worksheet.addRow([]);
  worksheet.addRow([]);
  
  worksheet.addRow(["STATE :" + this.Datas.dataStateName,'','DISTRICT :'+this.Datas.dataDistrictName,'']);
  worksheet.mergeCells('A4:B4');
  worksheet.addRow(['CLINIC TYPE :'+this.Datas.dataClinicTypeCode,'','PERIOD :'+ this.Datas.dataPeriod]);
  worksheet.mergeCells('A5:B5');
  
  worksheet.addRow(['YEAR :'+this.Datas.dataYear]);
  worksheet.addRow([]);
  worksheet.addRow([]);

  let header = worksheet.addRow(['#','BE Number','BE Value']);
  header.eachCell(cell => {
    cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
  })
  
  header.font={
    size : 12,
    bold:true,
  }
  
  let i =1;
  for(let d of this.equipmentList){
    //data=[];
    data.push([i,d.benumber,d.assetName]);
    i++;
  }
  for(let d of data){
    let row = worksheet.addRow(d);
    row.eachCell(cell => {
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
  }

  worksheet.getColumn(2).width = 18;
  worksheet.getColumn(3).width = 50;

  //footer:
  let footerRow = worksheet.addRow(['This is system generated excel sheet.']);

  let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    //Merge Cells
    worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
    worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
    worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);

  workbook.xlsx.writeBuffer().then((data) => {
    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    fs.saveAs(blob, 'EquipmentList_'+this.equipmentListType+'.xlsx');
  })

  }

  downloadPdfTotalEquipment(){
    // console.log('PDFTotalEquipment');
    let logo = this.Datas.logo;
    let equipListType = this.equipmentListType;
    let statePdf =this.Datas.dataStateName;
    let districtPdf = this.Datas.dataDistrictName;
    let clinicTypeCodePdf = this.Datas.dataClinicTypeCode;
    let periodPdf = this.Datas.dataPeriod;
    let yearPdf = this.Datas.dataYear;
    const document ={        
      pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
          columns:[
          {
            image: logo,
            fit: [275, 125],
            alignment: 'center',
            margin: [0, 0, 0, 10],
            
          },
        ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
        {
          text: '\nFIN-2B Equipment List -' + this.equipmentListType + '\n\n',
          style: 'header'
        },
  
        {
          text: this.Datas.dataStateName + ' State/ ' + this.Datas.dataDistrictName +' District/ ' + this.Datas.dataClinicTypeCode + ' / ' + this.Datas.dataPeriod + ' Period/ ' + this.Datas.dataYear + ' Year'+'\n',
          style: 'subHeader'
        },
  
        {
          style: 'table',
  
          table: {
            //widths:[100,'*',100,100,100],
            body: [
              [ {text : 'BE Number' , style : 'tableHead'},
               {text : 'BE Name' , style : 'tableHead'},
               {text : 'Existing Equipment' , style : 'tableHead'},
               {text : 'Added & Active' , style : 'tableHead'},
               {text : 'Removed & Deactivated' , style : 'tableHead'}
              ],
              ...this.equipmentList.map(x =>([x.benumber,x.assetName,x.cnd1,x.cnd2,x.cnd3])),
            ]
          },
          alignment : 'center',
        },
        
  
        ],

      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'left'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }

    }
    pdfMake.createPdf(document).download("FIN-2B-EquipmentList"+this.equipmentListType +".pdf");


  }

  generateExcelTotalEquipment(){
    
  // console.log('xcel start..');
  let workbook = new Workbook();
  let worksheet = workbook.addWorksheet('EquipmentList '+this.equipmentListType);
  let data = [];
  //console.log(data);

  const title = worksheet.addRow(['', '', 'FIN-2B EquipmentList '+ this.equipmentListType,'']);
  title.font={
    size: 12,
    bold:true
  }
  worksheet.mergeCells('D1:H1');
  worksheet.addRow([]);
  worksheet.addRow([]);
  
  worksheet.addRow(["STATE :" + this.Datas.dataStateName,'','DISTRICT :'+this.Datas.dataDistrictName,'']);
  worksheet.mergeCells('A4:B4');
  worksheet.addRow(['CLINIC TYPE :'+this.Datas.dataClinicTypeCode,'','PERIOD :'+ this.Datas.dataPeriod]);
  worksheet.mergeCells('A5:B5');
  
  worksheet.addRow(['YEAR :'+this.Datas.dataYear]);
  worksheet.addRow([]);
  worksheet.addRow([]);

  let header = worksheet.addRow(['#','BE Number','BE Value','Existing Equipment','Added & Active','Removed/Deactivated']);
  header.eachCell(cell => {
    cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
  })
  
  header.font={
    size : 12,
    bold:true,
  }
  
  let i =1;
  for(let d of this.equipmentList){
    //data=[];
    data.push([i,d.benumber,d.assetName,d.cnd1,d.cnd2,d.cnd3]);
    i++;
  }
  for(let d of data){
    let row = worksheet.addRow(d);
    row.eachCell(cell => {
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
  }

  worksheet.getColumn(2).width = 18;
  worksheet.getColumn(3).width = 50;

  //footer:
  let footerRow = worksheet.addRow(['This is system generated excel sheet.']);

  let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    //Merge Cells
    worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
    worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
    worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);

  workbook.xlsx.writeBuffer().then((data) => {
    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    fs.saveAs(blob, 'EquipmentList'+this.equipmentListType+'.xlsx');
  })
  }
  
}



//tada..