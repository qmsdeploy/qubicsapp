import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { Fin02bCreateEquipmentComponent } from "./fin-02b-create-equipment/fin-02b-create-equipment.component";
import { Fin02bViewEquipmentComponent } from "./fin-02b-view-equipment/fin-02b-view-equipment";

const routes: Routes = [
    {
        path:'',
        children: [
            { path: 'fin-02b-create-equipment', component: Fin02bCreateEquipmentComponent }        ]
    }
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []

})
export class Fin02bEquipmentRoutingModule { }
