import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { filter } from 'rxjs-compat/operator/filter';


@Injectable()

export class Fin02bEquipmentService {

  constructor(private http: HttpClient) { }

  serviceApiUrl: string = environment.serviceApiUrl;

  fetchStateDetails(){
    return this.http.get<Array<any>>(this.serviceApiUrl + 'state/all');
  }

  fetchEquipmentCount(){
    // console.log("service working");
    return this.http.get<Array<any>>(this.serviceApiUrl + 'cims-history-fin-02b-equip-count/equipment-count');
  }

  fetchEquipmentList(stateId,districtId,clinicTypeId,period,year){
    return this.http.get<Array<any>>(this.serviceApiUrl + 'cims-history-fin-02b-equipment/equipment-list/'+stateId
    +','+districtId+','+clinicTypeId+','+period+','+year);
  }
  
}