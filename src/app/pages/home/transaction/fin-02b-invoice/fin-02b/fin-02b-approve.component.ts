import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Fin02bInvoiceService } from '../fin-02b-invoice-service';
import * as dateFormat from 'dateformat';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { toWords } from 'number-to-words';
import { Fin02b } from '../model/fin02b';
import { CimsHistoryFin02b } from '../model/cimshistoryfin02b';
import pdfMake from "pdfmake/build/pdfmake.min";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import html2canvas from 'html2canvas';
import { formatNumber } from '@angular/common';
import { PagesService } from '../../../pages.service';
import { Subscription } from 'rxjs';
@Component({
    selector: 'fin-02b-approve',
    templateUrl: 'fin-02b-approve.component.html',
    styleUrls: ['fin-02b-approve.component.scss'],
    providers: [NgbModalConfig, NgbModal, CimsHistoryFin02b]
})

export class Fin02bApproveComponent implements OnInit {

    dtOptions: DataTables.Settings = {
        pagingType: 'full_numbers',
        pageLength: 10,
        lengthChange: false,
    };

    dtOptions1: DataTables.Settings = {
        pagingType: 'full_numbers',
        pageLength: 10,
        lengthChange: false,
        searching: false
    };

    public enableSaveButton: Boolean = window.history.state.approvalPage ? true : false;

    public fin02b: Fin02b;
    public cimsHistoryFin02b: CimsHistoryFin02b;
    public monthYear;
    public docRef = "";
    public date;
    public invoiceDate;
    public lastDate;
    public sstPercentage = 0;
    public invoiceTotalValueInWords: String;
    public invoiceTotalWholeValueInWords: String;
    public invoiceTotalDecimalValueInWords: String;
    public showSaveSubmitButton;
    public isApproved = false;
    public showButtons = false;
    public modalBodyContent;
    public loading: Boolean = true;
    public logo = ''
    public userId: Number;
    public totalinvoiceinwords: string;
    public invoiceTotalValueInWord: string;
    public storingPpd: any;
    public storingPkd: any;
    public swap: string = '';
    public district: any;
    public districtId: number;
    public clinicTypeId: number;
    public clinicAddress: string = '';
    public month: any;
    public clinicTypeCode: string;
    public districtName: string;
    public disableApprove: boolean;
    public show: any;
    public enabler: boolean
    public months: any;
    public year: any;
    public equipmentCount: any;
    public equipmentList: any;
    public tmp1: Subscription;
    public tmp2: Subscription;
    public xFlag: any;
    public yFlag: any;
    public equipmentListType: any;
    public pdfTitle: any;

    constructor(private router: Router, private route: ActivatedRoute, private fin02bService: Fin02bInvoiceService, 
        config: NgbModalConfig, private modalService: NgbModal, private pagesService: PagesService, private Data : CimsHistoryFin02b) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit() {
        this.xFlag = 0;
        this.yFlag = 0;
        this.disableApprove = false
        this.userId = this.pagesService.getUserId();
        this.fin02b = new Fin02b()
        this.cimsHistoryFin02b = new CimsHistoryFin02b()
        if (this.route.params['_value']['_id'] != "undefined") {
            this.route.params.switchMap((par: Params) => this.fin02bService.fetchDataForFin02bApprove(par['_id'])).subscribe(x => {
                this.fin02bService.findInvoiceSstPercentage(4).subscribe(y => {
                    this.sstPercentage = y;
                })
                this.fin02b = x;
                this.months = x.month
                this.year = x.year
                if (this.fin02b.clinicTypeCode == 'PPD') {
                    this.storingPpd = x
                    this.swap = "2"


                }
                else if (this.fin02b.clinicTypeCode == 'PKD') {
                    this.storingPkd = x
                    this.swap = "1"


                }

                if (this.fin02b.stateName == "SARAWAK") {
                  this.pdfTitle = "PKB / PPB";
                    if (this.fin02b.clinicTypeCode == "PKD") {
                        this.fin02b.clinicTypeCode = "PKB"
                    } else if (this.fin02b.clinicTypeCode == "PPD") {
                        this.fin02b.clinicTypeCode = "PPB"
                    }
                } else {
                  this.pdfTitle = "PKD / PPD";
                }
                // this.clinicTypeCode = this.fin02b.clinicTypeCode;
                this.clinicTypeId = this.fin02b.clinicTypeId
                this.districtId = this.fin02b.districtId
                // this.districtName = this.fin02b.districtName;

                this.cimsHistoryFin02b = x.cimsHistoryFin02b[0]
                this.monthYear = x.month + '/' + x.year;
                this.date = dateFormat(x.date, 'dd-mm-yyyy')
                this.invoiceDate = this.date
                this.docRef = this.fin02b.status == 'CREATED' || this.fin02b.status == 'SAVED' ? "" : this.fin02b.code;
                let transform = this.docRef
                let storing = transform.split("-")



                if (storing[4].length < 2) {
                    this.show = '0' + storing[4]
                }
                else {
                    this.show = storing[4]
                }
                this.isApproved = this.fin02b.status == 'APPROVED BY MOH' ? true : false;
                this.showButtons = this.fin02b.status == 'APPROVED BY MOH' ? false : true;
                this.showSaveSubmitButton = this.fin02b.status == 'IN INTERNAL APPROVAL' || this.fin02b.status == 'FOR APPROVAL TO MOH' ? false : true;
                if (this.fin02b.totalInvoiceValue % 1 == 0) {
                    this.invoiceTotalValueInWords = toWords(this.fin02b.totalInvoiceValue).toUpperCase();
                } else {
                    let totalAmountInString = this.fin02b.totalInvoiceValue.toString()
                    let splitValue = totalAmountInString.split('.')
                    this.invoiceTotalWholeValueInWords = toWords(splitValue[0]).toUpperCase();
                    this.invoiceTotalDecimalValueInWords = toWords(splitValue[1]).toUpperCase();
                    this.invoiceTotalValueInWords = this.invoiceTotalWholeValueInWords + ' AND CENTS ' + this.invoiceTotalDecimalValueInWords
                }

                this.fin02bService.fetchDistrictById(this.districtId).subscribe(x => {

                    this.district = x
                    if (this.swap == "1") {
                        this.clinicAddress = this.district.officePkdAddress;
                    } else if (this.swap == "2") {
                        this.clinicAddress = this.district.officePpdAddress;
                    }


                })
                this.loading = false;
            })
        }
    }

    openModal(content, modalContent) {
        this.invoiceDate = ''
        this.modalBodyContent = modalContent
        this.modalService.open(content);
    }

    closeModal() {
        if (this.modalBodyContent == 'Save') { this.saveStatus(); }
        if (this.modalBodyContent == 'Submit') { this.submitStatus(); }
        if (this.modalBodyContent == 'Approve') { this.updateStatus(); }
        if (this.modalBodyContent == 'invoiceDateChange') { this.downloadPdf() }
    }

    saveStatus() {
        delete this.fin02b.cimsHistoryFin02b;
        this.fin02b.status = "SAVED";
        this.fin02bService.updateFin02bStatus(this.fin02b).subscribe(x => {
            Swal.fire('', 'FIN 02b Invoice saved successfully!!!', 'success');
        })
    }

    submitStatus() {
        delete this.fin02b.cimsHistoryFin02b;
        this.fin02b.status = 'IN INTERNAL APPROVAL'
        this.fin02bService.updateFin02bStatus(this.fin02b).subscribe(x => {
            Swal.fire('', 'FIN 02b Invoice submitted successfully!!!', 'success');
            history.back()

        })
    }

    updateStatus() {
        this.disableApprove = true
        this.enabler = true
        if (this.fin02b.status == 'IN INTERNAL APPROVAL') { this.fin02b.status = 'FOR APPROVAL TO MOH', this.fin02b.approval1UserId = this.userId }
        else if (this.fin02b.status == 'FOR APPROVAL TO MOH') { this.fin02b.status = 'APPROVED BY MOH', this.fin02b.approval2UserId = this.userId }
        delete this.fin02b.cimsHistoryFin02b;
        this.fin02bService.updateFin02bStatus(this.fin02b).subscribe(x => {
            this.enabler = false
            this.disableApprove = false
            Swal.fire('', 'FIN 02B Invoice approved successfully!!!', 'success');
            history.back()
        })
    }

    navToList() {
        history.back()
    }

    openModal1(template,event,data){
        event.preventDefault();
        this.equipmentCount = [];
        if(this.xFlag != 0){
            this.tmp1.unsubscribe();
            this.xFlag += 1; 
        }
        let tmp: number = +data.month;
        data.month = tmp;
        if(data.month <= 4){
            data.period = "P1"
        }
        if(data.month >= 5 && data.month <= 8){
            data.period = "P2"
        }
        if(data.month >= 9){
            data.period = "P3"
        }

        // console.log('Data',data);

        this.tmp1 = this.fin02bService.fetchEquipmentCount().subscribe((x) => {  
            //  console.log(x);
            for(let element of x){
                //Need to include the Year Later... Done but let it be here..
                if(element.stateId == data.stateId && element.districtId == data.districtId && element.clinicTypeId == data.clinicTypeId && element.period == data.period && element.year == data.year){
                    //
                    this.Data.dataStateName = data.stateName;
                    this.Data.dataDistrictName = data.districtName;
                    this.Data.dataClinicTypeId = data.clinicTypeCode;
                    this.Data.dataPeriod = data.period;
                    this.Data.dataYear =data.year;

                    this.equipmentCount.push(element);
                    // this.Datas = element;
                    // console.log('Datas....'+ this.equipmentCount);
                }
            }
            // console.log('EquipmentCount',this.equipmentCount);
            this.modalService.open(template, {
                size: 'lg', centered: true,
            });
        });
    }

    openModal2(template,event,data){
        event.preventDefault();
        if(this.yFlag != 0){
            this.tmp2.unsubscribe();
            this.yFlag += 1; 
          }
        this.tmp2 = this.fin02bService.fetchEquipmentList(data.stateId,data.districtId,data.clinicTypeId,data.period,data.year).subscribe((x) => {    
            this.equipmentList = [];
            
            switch(template._declarationTContainer.localNames[0]){
                case 'template1' :
                  //
                  this.equipmentListType = 'Existing Equipment';
                  for(let element of x){
                    if(element.periodStatus == "EE"){
                      this.equipmentList.push(element)
                    }
                  }
                  break;
                case 'template2' :
                  //
                  this.equipmentListType = 'Added & Active';
                  for(let element of x){
                    if(element.periodStatus == "AA"){
                      this.equipmentList.push(element)
                    }
                  }
                  break;
                case 'template3' :
                  //
                  this.equipmentListType = 'Removed & Deactivated';
                  for(let element of x){
                    if(element.periodStatus == "RD"){
                      this.equipmentList.push(element)
                    }
                  }
                  break;
                case 'template4':
                    //
                    this.equipmentListType = '';
                    for(let element of x){
                      if(element.periodStatus == "EE"){
                        element.cnd1 = 'Y'
                        element.cnd2 = ''
                        element.cnd3 = ''
                        this.equipmentList.push(element)
                      }
                      if(element.periodStatus == "AA"){
                        element.cnd1 = ''
                        element.cnd2 = 'Y'
                        element.cnd3 = ''
                        this.equipmentList.push(element)
                      }
                      if(element.periodStatus == "RD"){
                        element.cnd1 = ''
                        element.cnd2 = ''
                        element.cnd3 = 'Y'
                        this.equipmentList.push(element)
                      }
                    }
                break;
                
      
              }
            this.modalService.open(template, {
                size: 'lg', centered: true,
            });
        });
    }

    public downloadPdf() {
        var data = document.getElementById("pdfImage");
        html2canvas(data, { scale: 1.5, scrollY: -window.scrollY }).then(canvas => {
            this.logo = canvas.toDataURL('image/png')
            this.generatePDF()
        });

    }

    generatePDF() {

        if (this.fin02b.status == 'FOR APPROVAL TO MOH') {
            if (this.invoiceDate != '') {
                this.invoiceDate = dateFormat(this.invoiceDate, 'dd-mm-yyyy')
                this.lastDate = this.invoiceDate
            }
        }

        let logo = this.logo

        let mySentence = this.invoiceTotalValueInWords;
        let words = mySentence.split(" ");

        for (let i = 0; i < words.length; i++) {
            words[i] = words[i].toLowerCase();
        }

        this.totalinvoiceinwords = words.join(" ");

        let mySentenc = this.totalinvoiceinwords;
        let word = mySentenc.split(" ");


        for (let i = 0; i < word.length; i++) {
            word[i] = word[i][0].toUpperCase() + word[i].substr(1);
        }
        this.invoiceTotalValueInWord = word.join(" ");

        if (this.fin02b.status == 'FOR APPROVAL TO MOH') {
            if (this.invoiceDate != '') {
                this.lastDate = this.invoiceDate
            }
        }
        else if (this.invoiceDate == '') {
            this.lastDate = ""
        }
        else {
            var d = new Date(this.year, this.months, 0);
            let data = new Date(d);
            this.lastDate = data.getDate() + '/' + (data.getMonth() + 1) + '/' + data.getFullYear()

        }

        let spa = this.invoiceTotalValueInWord.split("-");

        for (let i = 0; i < spa.length; i++) {
            spa[i] = spa[i][0].toUpperCase() + spa[i].substr(1);
        }
        this.invoiceTotalValueInWord = spa.join("-");


        var docDefinition = {
            pageMargins: [30, 135, 30, 60],
            header: function () {
                let header = [
                    {
                        image: logo,
                        fit: [275, 125],
                        alignment: 'center',
                        margin: [0, 0, 0, 10]
                    }
                ]
                return header;
            },
            footer: function (currentPage, pageCount) {
                let footerText = [
                    {
                        columns: [
                            {
                                text: 'QUANTUM MEDICAL SOLUTIONS SDN BHD (868557-V) ', alignment: 'center', width: '100%',
                            }
                        ]
                    },
                    {
                        columns: [
                            {
                                text: 'Unit L3A-6, Level 3A, Wisma Kemajuan, 2 Jalan 19/18, 46300 Petaling Jaya, TEL: +603 7629 6777 FAX: +603 7629 6779 ', alignment: 'center', width: '100%',
                            }
                        ]
                    },
                    {
                        columns: [
                            {
                                text: 'EMAIL: finance_AR@qms.com.my', alignment: 'center', width: '100%'
                            },
                        ]
                    },
                    {
                        columns: [
                            // { text: '**  This is a system generated document from Qubics, no signature is required.', alignment: 'center', width: '100%' },
                            { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', margin: [0, 0, 30, 0], width: '20%' },
                            { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', margin: [0, 0, 30, 0], width: '80%' }
                        ]
                    },
                ]
                return footerText;
            },
            content: [
                {
                    text: 'SST ID : W10-1904-32000110',
                    alignment: 'center', fontSize: 12, bold: true

                },
                {
                    table: {
                        widths: ['*'],
                        //widths: ['auto', 'auto', 'auto', 'auto', 'auto'],
                        body: [
                            [{
                                text: 'INVOICE FOR MAINTENANCE CHARGES',
                                bold: true
                            },],
                        ]
                    },
                    alignment: 'center',
                },
                {
                    table: {
                        body: [
                            [{
                                text: 'FOR ' + this.monthYear, bold: true,
                            },],
                        ]
                    },
                    margin: [235, 0, 0, 10],
                    alignment: 'center',
                },
                {
                    text: 'TAX INVOICES', bold: true, fontSize: 15,
                    alignment: 'center',
                },
                {
                    text: '\n'
                },
                {
                    columns: [
                        {
                            width: '55%',
                            stack: [
                                { text: 'INVOICE TO:', bold: true },
                                {
                                    table: {
                                        body: [
                                            [{
                                                table: {
                                                    widths: ['auto', 'auto', 'auto', 'auto'],
                                                    body: [

                                                        [{ text: this.pdfTitle, bold: true, fontSize: 9}, { text: ':', bold: true, alignment: 'right' }, { text: this.fin02b.clinicTypeCode, }, { text: this.fin02b.districtName, margin: [-95, 0, 0, 0] }],

                                                        [{ text: 'ADDRESS', bold: true }, { text: ':', bold: true, alignment: 'right' }, { text: this.clinicAddress, colSpan: 2 }, {}]

                                                    ],
                                                    alignment: 'center',
                                                }, layout: {
                                                    defaultBorder: false,

                                                },
                                            }]
                                        ]
                                    }
                                }
                            ],
                        },
                        {},
                        {
                            width: '40%',
                            stack: [
                                { text: 'FORM FIN 02-B', bold: true, alignment: 'right', fontSize: 12 },
                                {
                                    table: {
                                        widths: ['*', 'auto'],
                                        body: [
                                            [{ text: 'INVOICE REF:', bold: true }, { text: this.docRef, alignment: 'right', bold: true }],
                                            [{ text: 'DATE:', bold: true }, { text: this.lastDate, alignment: 'right', bold: true }]
                                        ],
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    text: '\n'
                },
                {
                    table: {
                        headerRows: 1,
                        widths: ['*', '*', '*', '*', '*'],
                        body: [
                            [{ text: this.pdfTitle + ' NAME', bold: true, alignment: 'center' }, { text: 'REFERENCE', bold: true, alignment: 'center' }, { text: 'GROSS CHARGES (RM)', bold: true, alignment: 'center' },
                            { text: 'PENALTY (RM)', bold: true, alignment: 'center' }, { text: 'NET CHARGES PAYABLE (RM)', bold: true, alignment: 'center' }],
                            [{ text: this.fin02b.clinicTypeCode + '-' + this.cimsHistoryFin02b.districtName, alignment: 'center' }, { text: 'BASE FEE ' + this.show, alignment: 'center' },
                            { text: formatNumber(this.cimsHistoryFin02b.totalEbeValue, 'en', '1.2-2'), alignment: 'right' }, { text: '-', alignment: 'center' },
                            { text: formatNumber(this.cimsHistoryFin02b.totalEbeValue, 'en', '1.2-2'), alignment: 'right' }],
                            // ['\n', '\n', '\n', '\n', '\n'],
                            // ['\n', '\n', '\n', '\n', '\n'],
                            // ['\n', '\n', '\n', '\n', '\n'],
                            [{ text: 'TOTAL INVOICE AMOUNT (Excluding SST)', colSpan: 2, bold: true }, '', { text: formatNumber(this.fin02b.invoiceBaseValue, 'en', '1.2-2'), bold: true, alignment: 'right' },
                            { text: '-', alignment: 'center' }, { text: formatNumber(this.fin02b.invoiceBaseValue, 'en', '1.2-2'), alignment: 'right', bold: true }],
                            [{ text: 'SST Amount (' + this.sstPercentage + '%)', colSpan: 4, bold: true, border: [false, false, false, false], }, '', '', '', { text: formatNumber(this.fin02b.sst, 'en', '1.2-2'), bold: true, alignment: 'right' }],
                            [{ text: 'TOTAL INVOICE AMOUNT (Inclusive SST)', colSpan: 4, bold: true, border: [false, false, false, false], }, '', '', '', { text: formatNumber(this.fin02b.totalInvoiceValue, 'en', '1.2-2'), bold: true, alignment: 'right' }],
                        ]
                    }
                },
                {
                    text: '\n'
                },
                {
                    text: '(Ringgit Malaysia: ' + this.invoiceTotalValueInWord + ' Only)', bold: true, italics: true
                },
                {
                    text: '\n'
                },
                {
                    text: '\n'
                },
                {
                    text: '\n'
                },
                {
                    columns: [
                        {
                            width: '100%',
                            table: {
                                widths: ['*', '*'],
                                body: [
                                    [{ text: 'REMITTANCE DETAILS', bold: true }, ''],
                                    [{ text: 'Payee:', bold: true, italics: true }, { text: 'QUANTUM MEDICAL SOLUTIONS SDN BHD', bold: true }],
                                    [{ text: 'Amount Due:', bold: true, italics: true }, { text: formatNumber(this.fin02b.totalInvoiceValue, 'en', '1.2-2'), bold: true }],
                                    [{ text: 'Bank Details:', bold: true, italics: true }, { text: 'RHB Bank Berhad', bold: true }],
                                    [{ text: 'Bank Account No:', bold: true, italics: true }, { text: '21219800038867', bold: true }],
                                ]
                            },
                            layout: 'lightHorizontalLines'
                        },
                        {}
                    ]
                },

                { text: 'This invoice is system generated and does not require a signature', bold: true, fontSize: 9, width: '100%' },

            ],
            pageSize: 'A4',
            pageOrientation: 'portrait',
            defaultStyle: {
                font: pdfMake.vfs.Roboto,
                fontSize: 10,
                lineHeight: 1
            },
            preserveSpace: {
                preserveLeadingSpaces: true
            }
        };

        pdfMake.createPdf(docDefinition).download('Invoice ' + this.docRef + '.pdf');
    }

    equipmentListDownloadPdf(event){
        event.preventDefault();
        let logo = this.Data.logo;
        // console.log('PDF');
    const document ={

        pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
          columns:[
          {
            image: logo,
            fit: [275, 125],
            alignment: 'center',
            margin: [0, 0, 0, 10],
            
          },
        ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
    //   {
    //     image: this.Data.logo,
    //     fit: [275, 125],
    //     alignment: 'center',
    //     margin: [0, -30, 0, 10]
    //   },

      {
        text: '\nFIN-2B Equipment List -' + this.equipmentListType + '\n',
        style: 'header'
      },

      {
        text: this.Data.dataStateName + ' State/ ' + this.Data.dataDistrictName +' District/ ' + this.Data.dataClinicTypeId + ' / ' + this.Data.dataPeriod + ' Period/ ' + this.Data.dataYear + ' Year'+'\n',
        style: 'subHeader'
      },

      {
        style: 'table',

        table: {
          widths:[100,'*'],
          body: [
            [ {text : 'BE Number' , style : 'tableHead'},
             {text : 'BE Name' , style : 'tableHead'}],
            ...this.equipmentList.map(x =>([x.benumber,x.assetName])),
          ]
        },
        alignment : 'left',
      },
      

      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'left'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }
    }
    pdfMake.createPdf(document).download("FIN-2B-EquipmentCount.pdf");

    }

    equipmentListGenerateExcel(event){
        event.preventDefault();
            //
            // console.log('xcel start..');
            let workbook = new Workbook();
            let worksheet = workbook.addWorksheet('EquipmentList -'+this.equipmentListType);
            let data = [];
            //console.log(data);
          
            const title = worksheet.addRow(['', '', 'FIN-2B EquipmentList -'+ this.equipmentListType,'']);
            title.font={
              size: 12,
              bold:true
            }
            worksheet.mergeCells('D1:H1');
            worksheet.addRow([]);
            worksheet.addRow([]);
            
            worksheet.addRow(["STATE :" + this.Data.dataStateName,'','DISTRICT :'+this.Data.dataDistrictName,'']);
            worksheet.mergeCells('A4:B4');
            worksheet.addRow(['CLINIC TYPE :'+this.Data.dataClinicTypeId,'','PERIOD :'+ this.Data.dataPeriod]);
            worksheet.mergeCells('A5:B5');
            
            worksheet.addRow(['YEAR :'+this.Data.dataYear]);
            worksheet.addRow([]);
            worksheet.addRow([]);
          
            let header = worksheet.addRow(['#','BE Number','BE Value']);
            header.eachCell(cell => {
              cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            })
            
            header.font={
              size : 12,
              bold:true,
            }
            
            let i =1;
            for(let d of this.equipmentList){
              //data=[];
              data.push([i,d.benumber,d.assetName]);
              i++;
            }
            for(let d of data){
              let row = worksheet.addRow(d);
              row.eachCell(cell => {
                cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
              })
            }
          
            worksheet.getColumn(2).width = 18;
            worksheet.getColumn(3).width = 30;
          
            //footer:
            let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
          
            let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
              footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
              //Merge Cells
              worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
              worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
              worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
          
            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
              fs.saveAs(blob, 'EquipmentList_'+this.equipmentListType+'.xlsx');
            })
          
            
    }

    equipmentListTotalDownloadPdf(event){
        event.preventDefault();
        let logo = this.Data.logo;
        // console.log('PDFTotal');
        const document ={
          
            pageMargins: [30, 135, 30, 60],
            header: function () {
              return{
                columns:[
                {
                  image: logo,
                  fit: [275, 125],
                  alignment: 'center',
                  margin: [0, 0, 0, 10],
                  
                },
              ]     
            }
            },
          footer: function (currentPage, pageCount) {
            let footerText = [
              {
                columns: [
                  {
                    stack: [
                      {
                        text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                        bold: true,
                        fontSize: 10,
                        // margin: [0, 0, 0, 10]
                      }
                    ], margin: [50, 0, 0, 0]
                  },
                  {
                    stack: [
                      {
                        text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                        bold: true,
                        fontSize: 10,
                        // margin: [0, 0, 0, 10]
                      }
                    ], margin: [30, 0, 0, 0]
                  },
                  {
                    stack: [
                      { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                    ], margin: [0, 0, 30, 0]
                  }
                ],
              }
            ]
            return footerText;
          },
    
          content: [
          // {
          //   image: this.Data.logo,
          //   fit: [275, 125],
          //   alignment: 'center',
          //   margin: [0, -30, 0, 10]
          // },
    
          {
            text: '\nFIN-2B Equipment List ' + this.equipmentListType + '\n',
            style: 'header'
          },
    
          {
            text: this.Data.dataStateName + ' State/ ' + this.Data.dataDistrictName +' District/ ' + this.Data.dataClinicTypeId + ' / ' + this.Data.dataPeriod + ' Period/ ' + this.Data.dataYear + ' Year'+'\n',
            style: 'subHeader'
          },
    
          {
            style: 'table',
    
            table: {
              body: [
                [ {text : 'BE Number' , style : 'tableHead'},
                 {text : 'BE Name' , style : 'tableHead'},
                 {text : 'Existing Equipment' , style : 'tableHead'},
                 {text : 'Added & Active' , style : 'tableHead'},
                 {text : 'Removed & Deactivated' , style : 'tableHead'}
                ],
                ...this.equipmentList.map(x =>([x.benumber,x.assetName,x.cnd1,x.cnd2,x.cnd3])),
              ]
            },
            alignment : 'center',
          },
          
    
          ],
          styles: {
            header: {
              fontSize: 18,
              bold: true,
              alignment : 'center'
            },
            tableHead:{
              bold : true,
              alignment : 'left'
            },
            subHeader:{
              fontSize:12,
              bold: true,
              alignment : 'center'
            },
          }
        }
        pdfMake.createPdf(document).download("FIN-2B-EquipmentCountTotal.pdf");
    
    }

    equipmentListTotalGenerateExcel(event){
        event.preventDefault();
        // console.log('xcel start..');
              let workbook = new Workbook();
              let worksheet = workbook.addWorksheet('EquipmentList '+this.equipmentListType);
              let data = [];
              //console.log(data);
            
              const title = worksheet.addRow(['', '', 'FIN-2B EquipmentList '+ this.equipmentListType,'']);
              title.font={
                size: 12,
                bold:true
              }
              worksheet.mergeCells('D1:H1');
              worksheet.addRow([]);
              worksheet.addRow([]);
              
              worksheet.addRow(["STATE :" + this.Data.dataStateName,'','DISTRICT :'+this.Data.dataDistrictName,'']);
              worksheet.mergeCells('A4:B4');
              worksheet.addRow(['CLINIC TYPE :'+this.Data.dataClinicTypeId,'','PERIOD :'+ this.Data.dataPeriod]);
              worksheet.mergeCells('A5:B5');
              
              worksheet.addRow(['YEAR :'+this.Data.dataYear]);
              worksheet.addRow([]);
              worksheet.addRow([]);
            
              let header = worksheet.addRow(['#','BE Number','BE Value','Existing Equipment','Added & Active','Removed/Deactivated']);
              header.eachCell(cell => {
                cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
              })
              
              header.font={
                size : 12,
                bold:true,
              }
              
              let i =1;
              for(let d of this.equipmentList){
                //data=[];
                data.push([i,d.benumber,d.assetName,d.cnd1,d.cnd2,d.cnd3]);
                i++;
              }
              for(let d of data){
                let row = worksheet.addRow(d);
                row.eachCell(cell => {
                  cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
                })
              }
            
              worksheet.getColumn(2).width = 18;
              worksheet.getColumn(3).width = 50;
            
              //footer:
              let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
            
              let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
                footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
                //Merge Cells
                worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
                worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
                worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
            
              workbook.xlsx.writeBuffer().then((data) => {
                let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                fs.saveAs(blob, 'EquipmentList_'+this.equipmentListType+'.xlsx');
              })
      }
}