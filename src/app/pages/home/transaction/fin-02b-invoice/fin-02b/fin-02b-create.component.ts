import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Fin02bInvoiceService } from '../fin-02b-invoice-service';
import * as dateFormat from 'dateformat';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { CimsHistoryFin02b } from '../model/cimshistoryfin02b';
import pdfMake from "pdfmake/build/pdfmake.min";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
// import { toWords } from 'number-to-words';
import { PagesService } from '../../../pages.service';
import { Subject, Subscription } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
    selector: 'fin-02b-create',
    templateUrl: './fin-02b-create.component.html',
    styleUrls: ['./fin-02b-create.component.scss'],
    providers: [NgbModalConfig, NgbModal, CimsHistoryFin02b]
})

export class Fin02bCreateComponent implements OnInit {

    dtOptions: DataTables.Settings = {
        pagingType: 'full_numbers',
        pageLength: 10,
        lengthChange: false,
        };
        
        dtOptions1: DataTables.Settings = {
        pagingType: 'full_numbers',
        pageLength: 10,
        lengthChange: false,
        searching: false
        };
        
        dtTrigger: Subject<any> = new Subject();
        @ViewChild(DataTableDirective, { static: false })
        dtElement: DataTableDirective;

    public clinicTypeCode: any;
    public districtName;
    public districtAddress = 'N/A';
    public monthYear;
    public netMaintenanceCharges;
    public chargesPayable;
    public grossCharges;
    public penaltyCharges;
    public netChargesPayable;
    public docRef = "";
    public date = ''
    public modalBodyContent;
    public cimsHistoryFin02b: CimsHistoryFin02b;
    public loading: Boolean = true;
    public disableCreate: Boolean = false;
    public userId: Number;
    public enabler: boolean;
    public xFlag:any;
    public yFlag:any;
    public equipmentList:any;
    public equipmentCount:any;
    public tmp1: Subscription;
    tmp2: Subscription;
    public equipmentListType: any;

    constructor(private router: Router, private route: ActivatedRoute, private fin02bService: Fin02bInvoiceService,
        config: NgbModalConfig, private modalService: NgbModal, private pagesService: PagesService, private Data : CimsHistoryFin02b) { }

    ngOnInit() {
        this.userId = this.pagesService.getUserId();
        if (this.route.params['_value'] != "undefined") {
            this.route.params.switchMap((par: Params) => this.fin02bService.fetchDataForFin02bCreate(par['_districtId'], par['_clinicTypeId'], par['_month'], par['_year'])).subscribe(x => {
                this.cimsHistoryFin02b = x;
                const dataa = JSON.stringify(this.cimsHistoryFin02b);
                // console.log("hahahhaha" + dataa);
                this.monthYear = this.cimsHistoryFin02b.month + '/' + this.cimsHistoryFin02b.year;
                this.date = dateFormat(new Date(), 'dd-mm-yyyy');
                this.loading = false;
                if (this.cimsHistoryFin02b.stateName == 'SARAWAK'){
                    if (this.cimsHistoryFin02b.clinicTypeId == 1) {
                        this.cimsHistoryFin02b.clinicTypeCode = 'PKB'
                    }
                    if (this.cimsHistoryFin02b.clinicTypeId == 2) {
                        this.cimsHistoryFin02b.clinicTypeCode = 'PPB'
                    }
                }
            })
            
        } 

        this.xFlag = 0;
        this.yFlag = 0;

    }

    openModal(content, modalContent) {
        this.modalBodyContent = modalContent
        this.modalService.open(content);
    }

    closeModal() {
        if (this.modalBodyContent == 'Create') { this.createFin02b(); }
    }

    createFin02b() {
        this.disableCreate = true
        this.enabler = true
        this.fin02bService.createFin02b(this.cimsHistoryFin02b, this.userId).subscribe(x => {
            this.disableCreate = false
            this.enabler = false
            Swal.fire('', 'FIN 02B Invoice created and submitted successfully!!!', 'success');
            //  this.router.navigateByUrl('/transaction/fin-02b-invoice/fin-02b-create-list')
            history.back()
        })
    }

    navToList() {
        // this.router.navigate(['/transaction/fin-02b-invoice/fin-02b-create-list'])
        history.back()
    }

    openModal1(template,event,data){
        event.preventDefault();
        this.equipmentCount = [];
        if(this.xFlag != 0){
            this.tmp1.unsubscribe();
            this.xFlag += 1; 
        }
        let tmp: number = +data.month;
        data.month = tmp;
        if(data.month <= 4){
            data.period = "P1"
        }
        if(data.month >= 5 && data.month <= 8){
            data.period = "P2"
        }
        if(data.month >= 9){
            data.period = "P3"
        }

        // console.log('Data',data);

        this.tmp1 = this.fin02bService.fetchEquipmentCount().subscribe((x) => {  
            //  console.log(x);
            for(let element of x){
                //Need to include the Year Later... Done but let it be here
                if(element.stateId == data.stateId && element.districtId == data.districtId && element.clinicTypeId == data.clinicTypeId && element.period == data.period && element.year == data.year){
                    //
                    this.Data.dataStateName = data.stateName;
                    this.Data.dataDistrictName = data.districtName;
                    this.Data.dataClinicTypeId = data.clinicTypeCode;
                    this.Data.dataPeriod = data.period;
                    this.Data.dataYear =data.year;
                    
                    this.equipmentCount.push(element);
                }
            }
            // console.log('EquipmentCount',this.equipmentCount);
            this.modalService.open(template, {
                size: 'lg', centered: true,
            });
        });
    }

    openModal2(template,event,data){
        event.preventDefault();
        if(this.yFlag != 0){
            this.tmp2.unsubscribe();
            this.yFlag += 1; 
          }
        this.tmp2 = this.fin02bService.fetchEquipmentList(data.stateId,data.districtId,data.clinicTypeId,data.period,data.year).subscribe((x) => {    
            this.equipmentList = [];
            
            //
            switch(template._declarationTContainer.localNames[0]){
                case 'template1' :
                  //
                  this.equipmentListType = 'Existing Equipment';
                  for(let element of x){
                    if(element.periodStatus == "EE"){
                      this.equipmentList.push(element)
                    }
                  }
                break;
                case 'template2' :
                  //
                  this.equipmentListType = 'Added & Active';
                  for(let element of x){
                    if(element.periodStatus == "AA"){
                      this.equipmentList.push(element)
                    }
                  }
                break;
                case 'template3' :
                  //
                  this.equipmentListType = 'Removed & Deactivated';
                  for(let element of x){
                    if(element.periodStatus == "RD"){
                      this.equipmentList.push(element)
                    }
                  }
                break;
                case 'template4':
                  //
                  this.equipmentListType = '';
                  for(let element of x){
                    if(element.periodStatus == "EE"){
                      element.cnd1 = 'Y'
                      element.cnd2 = ''
                      element.cnd3 = ''
                      this.equipmentList.push(element)
                    }
                    if(element.periodStatus == "AA"){
                      element.cnd1 = ''
                      element.cnd2 = 'Y'
                      element.cnd3 = ''
                      this.equipmentList.push(element)
                    }
                    if(element.periodStatus == "RD"){
                      element.cnd1 = ''
                      element.cnd2 = ''
                      element.cnd3 = 'Y'
                      this.equipmentList.push(element)
                    }
                  }
                break;      
              }
            this.modalService.open(template, {
                size: 'lg', centered: true,
            });
        });
    }

    equipmentListDownloadPdf(event){
        event.preventDefault();
        let logo = this.Data.logo;
        // console.log('PDF');
    const document ={

      pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
          columns:[
          {
            image: logo,
            fit: [275, 125],
            alignment: 'center',
            margin: [0, 0, 0, 10],
            
          },
        ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
      // {
      //   image: this.Data.logo,
      //   fit: [275, 125],
      //   alignment: 'center',
      //   margin: [0, -30, 0, 10]
      // },

      {
        text: '\nFIN-2B Equipment List -' + this.equipmentListType + '\n',
        style: 'header'
      },

      {
        text: this.Data.dataStateName + ' State/ ' + this.Data.dataDistrictName +' District/ ' + this.Data.dataClinicTypeId + ' / ' + this.Data.dataPeriod + ' Period/ ' + this.Data.dataYear + ' Year'+'\n',
        style: 'subHeader'
      },

      {
        style: 'table',

        table: {
          body: [
            [ {text : 'BE Number' , style : 'tableHead'},
             {text : 'BE Name' , style : 'tableHead'}],
            ...this.equipmentList.map(x =>([x.benumber,x.assetName])),
          ]
        },
        alignment : 'left',
      },
      

      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'left'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }
    }
    pdfMake.createPdf(document).download("FIN-2B-EquipmentCount.pdf");

    }

    equipmentListGenerateExcel(event){
        event.preventDefault();
            //
            // console.log('xcel start..');
            let workbook = new Workbook();
            let worksheet = workbook.addWorksheet('EquipmentList -'+this.equipmentListType);
            let data = [];
            //console.log(data);
          
            const title = worksheet.addRow(['', '', 'FIN-2B EquipmentList -'+ this.equipmentListType,'']);
            title.font={
              size: 12,
              bold:true
            }
            worksheet.mergeCells('D1:H1');
            worksheet.addRow([]);
            worksheet.addRow([]);
            
            worksheet.addRow(["STATE :" + this.Data.dataStateName,'','DISTRICT :'+this.Data.dataDistrictName,'']);
            worksheet.mergeCells('A4:B4');
            worksheet.addRow(['CLINIC TYPE :'+this.Data.dataClinicTypeId,'','PERIOD :'+ this.Data.dataPeriod]);
            worksheet.mergeCells('A5:B5');
            
            worksheet.addRow(['YEAR :'+this.Data.dataYear]);
            worksheet.addRow([]);
            worksheet.addRow([]);
          
            let header = worksheet.addRow(['#','BE Number','BE Value']);
            header.eachCell(cell => {
              cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            })
            
            header.font={
              size : 12,
              bold:true,
            }
            
            let i =1;
            for(let d of this.equipmentList){
              //data=[];
              data.push([i,d.benumber,d.assetName]);
              i++;
            }
            for(let d of data){
              let row = worksheet.addRow(d);
              row.eachCell(cell => {
                cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
              })
            }
          
            worksheet.getColumn(2).width = 18;
            worksheet.getColumn(3).width = 30;
          
            //footer:
            let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
          
            let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
              footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
              //Merge Cells
              worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
              worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
              worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
          
            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
              fs.saveAs(blob, 'EquipmentList_'+this.equipmentListType+'.xlsx');
            })
          
            
    }
   
    equipmentListTotalDownloadPdf(event){
      event.preventDefault();
      let logo = this.Data.logo;
    //   console.log('PDFTotal');
      const document ={
        
        pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
          columns:[
          {
            image: logo,
            fit: [275, 125],
            alignment: 'center',
            margin: [0, 0, 0, 10],
            
          },
        ]     
      }
      },
        footer: function (currentPage, pageCount) {
          let footerText = [
            {
              columns: [
                {
                  stack: [
                    {
                      text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                      bold: true,
                      fontSize: 10,
                      // margin: [0, 0, 0, 10]
                    }
                  ], margin: [50, 0, 0, 0]
                },
                {
                  stack: [
                    {
                      text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                      bold: true,
                      fontSize: 10,
                      // margin: [0, 0, 0, 10]
                    }
                  ], margin: [30, 0, 0, 0]
                },
                {
                  stack: [
                    { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                  ], margin: [0, 0, 30, 0]
                }
              ],
            }
          ]
          return footerText;
        },
  
        content: [
        // {
        //   image: this.Data.logo,
        //   fit: [275, 125],
        //   alignment: 'center',
        //   margin: [0, -30, 0, 10]
        // },
  
        {
          text: '\nFIN-2B Equipment List ' + this.equipmentListType + '\n',
          style: 'header'
        },
  
        {
          text: this.Data.dataStateName + ' State/ ' + this.Data.dataDistrictName +' District/ ' + this.Data.dataClinicTypeId + ' / ' + this.Data.dataPeriod + ' Period/ ' + this.Data.dataYear + ' Year'+'\n',
          style: 'subHeader'
        },
  
        {
          style: 'table',
  
          table: {
            body: [
              [ {text : 'BE Number' , style : 'tableHead'},
               {text : 'BE Name' , style : 'tableHead'},
               {text : 'Existing Equipment' , style : 'tableHead'},
               {text : 'Added & Active' , style : 'tableHead'},
               {text : 'Removed & Deactivated' , style : 'tableHead'}
              ],
              ...this.equipmentList.map(x =>([x.benumber,x.assetName,x.cnd1,x.cnd2,x.cnd3])),
            ]
          },
          alignment : 'center',
        },
        
  
        ],
        styles: {
          header: {
            fontSize: 18,
            bold: true,
            alignment : 'center',
          },
          tableHead:{
            bold : true,
            alignment : 'left',
          },
          subHeader:{
            fontSize:12,
            bold: true,
            alignment : 'center',
          },
        }
      }
      pdfMake.createPdf(document).download("FIN-2B-EquipmentCountTotal.pdf");
  
    }

    equipmentListTotalGenerateExcel(event){
      event.preventDefault();
    //   console.log('xcel start..');
            let workbook = new Workbook();
            let worksheet = workbook.addWorksheet('EquipmentList '+this.equipmentListType);
            let data = [];
            //console.log(data);
          
            const title = worksheet.addRow(['', '', 'FIN-2B EquipmentList '+ this.equipmentListType,'']);
            title.font={
              size: 12,
              bold:true
            }
            worksheet.mergeCells('D1:H1');
            worksheet.addRow([]);
            worksheet.addRow([]);
            
            worksheet.addRow(["STATE :" + this.Data.dataStateName,'','DISTRICT :'+this.Data.dataDistrictName,'']);
            worksheet.mergeCells('A4:B4');
            worksheet.addRow(['CLINIC TYPE :'+this.Data.dataClinicTypeId,'','PERIOD :'+ this.Data.dataPeriod]);
            worksheet.mergeCells('A5:B5');
            
            worksheet.addRow(['YEAR :'+this.Data.dataYear]);
            worksheet.addRow([]);
            worksheet.addRow([]);
          
            let header = worksheet.addRow(['#','BE Number','BE Value','Existing Equipment','Added & Active','Removed/Deactivated']);
            header.eachCell(cell => {
              cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            })
            
            header.font={
              size : 12,
              bold:true,
            }
            
            let i =1;
            for(let d of this.equipmentList){
              //data=[];
              data.push([i,d.benumber,d.assetName,d.cnd1,d.cnd2,d.cnd3]);
              i++;
            }
            for(let d of data){
              let row = worksheet.addRow(d);
              row.eachCell(cell => {
                cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
              })
            }
          
            worksheet.getColumn(2).width = 18;
            worksheet.getColumn(3).width = 50;
          
            //footer:
            let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
          
            let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
              footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
              //Merge Cells
              worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
              worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
              worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
          
            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
              fs.saveAs(blob, 'EquipmentList_'+this.equipmentListType+'.xlsx');
            })
    }
}