import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import "rxjs/add/operator/switchMap";
import { PAIService } from "./pai-service";
import * as dateFormat from "dateformat";
import { Invoice } from "./model/invoice";
import { InvoicePaymentHistory } from "./model/invoice-payment-history";
import "sweetalert2/src/sweetalert2.scss";
import Swal from "sweetalert2";
import { DatePipe } from '@angular/common';

@Component({
  selector: "pai-update",
  templateUrl: "./pai-update.component.html",
  styleUrls: ["./pai-update.component.scss"],
})
export class updatedetailsComponent implements OnInit {
  public invoice: Invoice;
  public invoicePayment: InvoicePaymentHistory;
  public loading: Boolean = true;
  public today = dateFormat(new Date(), "yyyy-mm-dd");
  public disableSubmit: Boolean = false;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private paiService: PAIService,
    private datepipe: DatePipe
  ) { }

  ngOnInit() {
    this.invoice = new Invoice();
    this.invoicePayment = new InvoicePaymentHistory();
    if (this.route.params["_value"]["_id"] != "undefined") {
      this.route.params
        .switchMap((par: Params) =>
          this.paiService.fetchForPAIUpdate(par["_id"])
        )
        .subscribe((x) => {
          this.invoice = x;
          if (this.invoice.invoiceDate == null) {
            this.invoice.invoiceDateDisplay = null;
          } else {
            this.invoice.invoiceDateDisplay = dateFormat(
              this.invoice.invoiceDate,
              "dd-mm-yyyy"
            );
          }
          this.loading = false;
        });
    }
    this.disableSubmit = false;
    this.invoicePayment.paymentDate = null
  }

  // disableSubmit() {
  //   if (this.invoice.outstandingAmount != this.invoice.paymentReceived) {
  //     return true
  //   } else {
  //   return false
  //   }
  // }


  updateAsDraft() {
    delete this.invoice.fin06;
    delete this.invoice.fin10b;
    delete this.invoice.fin03a;
    this.invoice.paymentStatus = "PAYMENT-DRAFT";
    this.paiService.updateInvoice(this.invoice).subscribe((x) => {
      this.router.navigateByUrl("transaction/pai/pai-list");
    });
  }

  save(event: any) {
    console.log(this.invoicePayment.paymentDate)
    if (this.invoice.invoiceNo) {
      this.paiService
        .invoicePaymentFirstCheck(this.invoice.invoiceNo)
        .subscribe((x) => {
          // console.log(x);
          if (x) {
            this.afterSave(event);
          } else {
            Swal.fire("", "Outstanding Amount is 0");
            this.router.navigateByUrl("transaction/pai/pai-list");
          }
        });
    }
  }

  afterSave(event: any) {
    // console.log("After save start")
    // event.target.disabled = true;
    if (
      this.invoicePayment.paymentRefNo == undefined ||
      this.invoicePayment.paymentReceived == 0 ||
      this.invoicePayment.paymentRefNo == "" || 
      this.invoicePayment.paymentDate === null
    ) {
      Swal.fire("", "Please fill all fields", "error");
    } else if (
      this.invoicePayment.paymentReceived > this.invoice.outstandingAmount
    ) {
      Swal.fire(
        "",
        "Collection Receiving Amount can not be more than Outstanding Amount",
        "error"
      );
    }
    
    else {
      this.invoicePayment.invoiceNo = this.invoice.invoiceNo;
      // this.invoicePayment.paymentDate = new Date();
      this.invoicePayment.paymentMode = "Bank Receipting";
      this.invoicePayment.updatedBy = JSON.parse(
        localStorage.getItem("currentUser")
      ).id;
      let textFireHead = "<b>Do you want to update :</b> " + "<br><br>";
      // let textFireInvoiceNo = 'Invoice Ref No : ' + this.invoicePayment.invoiceNo + '<br>'
      // let textFireBankRefNo = 'Bank Ref No : ' + this.invoicePayment.paymentRefNo + '<br>'
      // let textFireCollectionReceivingNow = 'Collection Receiving Now(RM) : ' + this.invoicePayment.paymentReceived + '<br>'
      // let textFireCollectionReceivingDate = 'Collection Receiving Date : ' + this.datepipe.transform(this.invoicePayment.paymentDate,'dd/MM/yyyy') + '<br>'
      let textFireTableHead = `<tr><th style=" border: 1px solid black">Invoice Ref No</th><th style=" border: 1px solid black">Bank Ref No</th><th style=" border: 1px solid black">Collection Receiving Now(RM)</th><th style=" border: 1px solid black">Collection Receiving Date</th></tr>`;
      let textFireTableBody =
        `<tr><td style=" border: 1px solid black">` +
        this.invoicePayment.invoiceNo +
        `</td><td style=" border: 1px solid black">` +
        this.invoicePayment.paymentRefNo +
        `</td><td style=" border: 1px solid black">` +
        this.invoicePayment.paymentReceived.toLocaleString("en-US") +
        `</td><td style=" border: 1px solid black">` +
        this.datepipe.transform(this.invoicePayment.paymentDate, 'dd/MM/yyyy') + `</td></tr>`

      Swal.fire({
        title: "Are you sure?",
        html:
          textFireHead +
          "<table>" +
          textFireTableHead +
          textFireTableBody +
          "</table>",
        // html: textFireHead + textFireInvoiceNo + textFireBankRefNo + textFireCollectionReceivingNow + textFireCollectionReceivingDate,
        // text: '',
        // icon: 'warning',
        showCancelButton: true,
        confirmButtonText: "Submit",
        cancelButtonText: "Cancel",
      }).then((result) => {
        // console.log(result.value)
        if (result.value) {
          event.target.disabled = true;
          this.paiService.invoicePayment(this.invoicePayment).subscribe((x) => {
            Swal.fire(
              "",
              "Payment Received Successfully. Transaction Reference No - " +
              x.transactionRefNo,
              "success"
            );
            history.back();
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          this.disableSubmit = false;
          Swal.fire("", "Transaction has been cancelled.");
        }
      }); //End of Conformation Pop Up:
    }
  }

  action() {
    this.router.navigateByUrl("transaction/pai/pai-list");
  }
}
