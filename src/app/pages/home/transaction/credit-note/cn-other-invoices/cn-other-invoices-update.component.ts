import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { PAIService } from '../../payment-against-invoice/pai-service';
import * as dateFormat from 'dateformat';
import { Invoice } from '../../payment-against-invoice/model/invoice';
import { InvoicePaymentHistory } from '../../payment-against-invoice/model/invoice-payment-history';
import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'cn-other-invoices-update',
  templateUrl: './cn-other-invoices-update.component.html',
  styleUrls: ['./cn-other-invoices-update.component.scss'],
  providers: [DatePipe]
})
export class CnOtherInvoicesUpdateComponent implements OnInit {

  public invoice: Invoice;
  public invoicePayment: InvoicePaymentHistory;
  public loading: Boolean = true;
  public today = dateFormat(new Date(), 'yyyy-mm-dd');
  public disableSubmit: Boolean = false;


  constructor(public router: Router, private route: ActivatedRoute, private paiService: PAIService, private datepipe: DatePipe) { }

  ngOnInit() {
    this.invoice = new Invoice();
    this.invoicePayment = new InvoicePaymentHistory();
    if (this.route.params['_value']['_id'] != "undefined") {
      this.route.params.switchMap((par: Params) => this.paiService.fetchForPAIUpdate(par['_id'])).subscribe(x => {
        this.invoice = x;

        if (this.invoice.invoiceDate == null) { this.invoice.invoiceDateDisplay = null }
        else {
          this.invoice.invoiceDateDisplay = dateFormat(this.invoice.invoiceDate, 'dd-mm-yyyy')
        }
        this.loading = false;
      })
    }
  }

  // disableSubmit() {
  //   if (this.invoice.outstandingAmount != this.invoice.paymentReceived) {
  //     return true
  //   } else {
  //   return false
  //   }
  // }

  updateAsDraft() {
    delete this.invoice.fin06
    delete this.invoice.fin10b
    delete this.invoice.fin03a
    this.invoice.paymentStatus = 'PAYMENT-DRAFT'
    this.paiService.updateInvoice(this.invoice).subscribe(x => {
      this.router.navigateByUrl('transaction/credit-note/cn-other-invoices/cn-other-invoices');
    })
  }



  save() {

    let tmp = this.invoicePayment.paymentReceived * (this.invoice.sst / this.invoice.invoiceBaseValue)
    console.log('tmp Value', tmp)
    let sst = (Math.round(tmp * 100)) / 100
    let netAmt = (Math.round((this.invoicePayment.paymentReceived + sst) *100)) / 100
    //let netAmt = this.invoicePayment.paymentReceived + sst


    if (this.invoicePayment.paymentRefNo == undefined || this.invoicePayment.paymentReceived == null || this.invoicePayment.paymentReceived == 0 || this.invoicePayment.paymentRefNo == "" || this.invoicePayment.paymentDate == undefined) {
      Swal.fire('', 'Please fill all fields', 'error');
    } else if (netAmt > this.invoice.outstandingAmount && this.invoicePayment.paymentReceived <= this.invoice.outstandingAmount){
      Swal.fire('','Collection Receiving Amount including the SST can not be more than Outstanding Amount','warning')
    }
    else if (this.invoicePayment.paymentReceived > this.invoice.outstandingAmount) {
      Swal.fire('', 'Collection Receiving Amount can not be more than Outstanding Amount', 'warning');
    }
    else {

      //formula:
      // let tmp = this.invoicePayment.paymentReceived * (this.invoice.sst / this.invoice.invoiceBaseValue)
      // let sst = Math.round(tmp * 100) / 100;



      console.log('SSt', sst, 'Credit note Generating now..', this.invoicePayment.paymentReceived, 'Gsst', this.invoice.sst, 'BaseValue', this.invoice.invoiceBaseValue)

      this.invoicePayment.invoiceNo = this.invoice.invoiceNo
      this.invoicePayment.paymentMode = "Credit Note"
      this.invoicePayment.updatedBy = JSON.parse(localStorage.getItem('currentUser')).id

      let textFireHead = '<b>Do you want to update :</b> ' + '<br><br>'
      let textFireTableHead = `<tr><th style=" border: 1px solid black">Credit Note Invoice</th><th style=" border: 1px solid black">Credit Note Generating Now</th><th style=" border: 1px solid black">SST</th><th style=" border: 1px solid black">Net Amount</th><th style=" border: 1px solid black">Remarks</th><th style=" border: 1px solid black">Credit Note Generating Date</th></tr>`
      let textFireTableBody = `<tr><td style=" border: 1px solid black">` + this.invoicePayment.invoiceNo + `</td><td style=" border: 1px solid black">` + this.invoicePayment.paymentReceived.toLocaleString("en-US") + `</td><td style=" border: 1px solid black">` + sst + `</td><td style=" border: 1px solid black">` + netAmt + `</td><td style=" border: 1px solid black">` + this.invoicePayment.paymentRefNo + `</td><td style=" border: 1px solid black">` + this.datepipe.transform(this.invoicePayment.paymentDate, 'dd/MM/yyyy') + `</td></tr>`

      Swal.fire({
        title: 'Are you sure?',
        html: textFireHead + '<table>' + textFireTableHead + textFireTableBody + '</table>',
        showCancelButton: true,
        confirmButtonText: 'Submit',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result.value) {

          //formula add up:
          this.invoicePayment.paymentReceived = netAmt;


          this.paiService.invoicePayment(this.invoicePayment).subscribe(x => {
            Swal.fire('', 'Payment Received Successfully. Transaction Reference No - ' + x.transactionRefNo, 'success');
            history.back();
          })

        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire('', 'Transaction has been cancelled.')
        }
      })//End of Conformation Pop Up:

    }

  }

  action() {
    this.router.navigateByUrl('transaction/credit-note/cn-other-invoices/cn-other-invoices');
  }

}
