import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
//import { environment } from '../../../../../../../environments/environment';
// import { toWords } from 'number-to-words';
//import { dateformat } from 'dateformat';
//import { Fin01InvoiceService } from "../../fin-01-invoice-service";
import { element } from 'protractor';
import { DataTableDirective } from 'angular-datatables';
// import { PAIService } from '../../payment-against-invoice/pai-service';
import { Fin09InvoiceService } from '../../fin-09-invoice/fin-09-invoice-service';
import Swal from 'sweetalert2';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Fin09ABCDE } from '../../fin-09-invoice/model/fin09-abcde';
import { PAIService } from '../../payment-against-invoice/pai-service';
import { FLOAT } from 'html2canvas/dist/types/css/property-descriptors/float';
import { InvoiceCounter } from '../../fin-01-invoice/model/invoice-counter';
import { timeStamp } from 'console';

var dateFormat = require('dateformat');

@Component({
    selector: 'fin09-credit-note-created-list',
    templateUrl: './created-list-details.component.html',

})

export class CreatedListDetailsComponent implements OnInit {

    public searchText: string;
    public listDatas: any;
    public filterDatas: Array<any>;
    public stateFilter: string;
    public districtFilter: string;
    public invoiceTypeName: any;
    public clinicTypeCode: any;
    public stateFilterDatas: Array<any>;
    public districtFilterDatas: Array<any>;
    public loading: Boolean = false;
    public sumAmt: any = [];
    public sum: FLOAT;
    public sums: any;
    public paymentRefNo;
    public rupee = "RS";
    public payList: any = [];
    public payrefnoList: any = [];
    public finGroups: any = [];
    public modalBodyContent;
    public disableCreate: Boolean = false;
    public totalRetention: number = 1000;
    // finGroupsList is the final payment array...
    public finGroupsList: any = []
    clicked = false;
    public fin09sd: Fin09ABCDE;
    public approvalQuater: any;
    public approvalYear: any;
    public month: any;
    public checking: any;
    public storings: Array<any>;
    public gSst : any;
    public penaltyAdjTotal : any;
    public sstTotal : any;
    public penaltyAdjNowTotal : any;
    public totalPenalty : any;
    rententionTotal: any;
    totalAmtcheckWithPenaltyFlag: boolean;



    constructor(private router: Router, config: NgbModalConfig, private route: ActivatedRoute, private modalService: NgbModal, public paiService: PAIService, public fin09service: Fin09InvoiceService) { }

    dtOptions: DataTables.Settings = {
        pagingType: 'full_numbers',
        pageLength: 10
    };
    dtTrigger: Subject<any> = new Subject();
    @ViewChild(DataTableDirective, { static: false })
    dtElement: DataTableDirective;
    ngOnInit() {
        this.sums = 0;
        this.gSst = 0;
        if (this.route.params['_value']['_id'] != "undefined") {
            this.route.params.switchMap((par: Params) => this.fin09service.fetchDataForFin09Approve(par['_id'])).subscribe(x => {

                // // console.log("The service data is fetched from Fin09 Service..",x);
                this.fin09sd = x;
                this.stateFilter = this.fin09sd.stateName;
                this.districtFilter = this.fin09sd.districtName
                this.sums = this.fin09sd.totalPenalty
                this.paymentRefNo = this.fin09sd.code;
                this.month = this.fin09sd.month
                this.clinicTypeCode = this.fin09sd.clinicTypeId
                this.approvalQuater = this.fin09sd.approvalQuater
                this.approvalYear = parseInt(this.fin09sd.approvalYear)
                // // console.log('this.sums :',this.sums)
                this.totalPenalty = this.sums;

            })
        }
        //this.filterFunction();

        setTimeout(() => {
            this.filterFunction()
        }, 950)

    }

    onTick(id: number, e,data) {
        // // console.log('data....',data)
        if (e.target.checked) {
            this.filterDatas.forEach
                (invoice => {
                    if (invoice.id == id) {
                        //For enabling the text field...
                        invoice.disableCollectionAmount = false
                        invoice.disableCollectionRefNo = false
                        invoice.enableListData = e.target.checked
                    }
                })
            let search = this.filterDatas.find(x => x.id === id)
            this.finGroups.push(search)
        }
        else {
            this.filterDatas.forEach(invoice => {
                if (invoice.id == id) {
                    invoice.disableCollectionAmount = true
                    invoice.disableCollectionRefNo = true
                    invoice.enableListData = e.target.checked
                    invoice.amountReceived = "";
                }
            })
            let search = this.finGroups.find(x => x.id === id)
            let index = this.finGroups.indexOf(search, 0)
            this.finGroups.splice(index, 1)
        }
        this.handleTotalAmountPaid();
    }


    handleTotalAmountPaid() {
        let amountList = [];
        this.filterDatas.forEach(invoice => {
            // // console.log("Filter Datas : ",this.filterDatas)
            if (invoice.enableListData) {
                // amountList.push(invoice.amountReceived);
                //sst calc
                let sstCheck = Math.round((invoice.invoiceBaseValue * 0.06)*100)/100;
                let sstTmp = 0;
                let removeSstTmp = 0;
                let addSstTmp = 0;
                // // console.log('SSSTT : ',);

                // new code hope finalized:
                //  this is for the 50 to 53.19 thing
                if(this.totalPenalty < this.rententionTotal){

                    if(sstCheck <= (invoice.sst + 1) &&  sstCheck >= (invoice.sst - 1)){
                        // // console.log('Check this sst 6% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                        // removeSstTmp = (invoice.amountReceived *100)/106;
                        // sstTmp = invoice.amountReceived * 0.06;
                        // addSstTmp = invoice.amountReceived + sstTmp;

                        // addSstTmp = invoice.amountReceived / 0.94;
                        addSstTmp =invoice.amountReceived;
                        sstTmp  = addSstTmp * 0.06;
                        // // console.log("SSSSTT - 6%",sstTmp)
                        if(sstTmp >= 0.4){
                        // if(this.totalPenalty >18){
                            // amountList.push((invoice.amountReceived *100)/106)
                            // amountList.push((invoice.amountReceived * 0.06) + invoice.amountRecived)
                            amountList.push(addSstTmp);
                        }else{
                            amountList.push(invoice.amountReceived)
                        }
                                
                    }else{
                        // // console.log('Check this sst 8% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                        // removeSstTmp = (invoice.amountReceived *100)/108 ;
                        // sstTmp = invoice.amountReceived * 0.08;
                        // addSstTmp = invoice.amountReceived + sstTmp;

                        // addSstTmp = invoice.amountReceived / 0.92;
                        addSstTmp = invoice.amountReceived;
                        sstTmp  = addSstTmp * 0.08;
                        // // console.log("SSSSTT - 8%",sstTmp)
                        if(sstTmp >= 0.4){
                        // if(this.totalPenalty > 18){
                            // amountList.push((invoice.amountReceived *100)/108)
                            // amountList.push((invoice.amountReceived * 0.08) + invoice.amountReceived)
                            amountList.push(addSstTmp);
                        }else{
                            amountList.push(invoice.amountReceived)
                        }                    
                    }

                    console.log("amountList :",amountList)

                }else if(this.totalPenalty > this.rententionTotal){

                    console.log("else if i s working...")

                    if(sstCheck <= (invoice.sst + 1) &&  sstCheck >= (invoice.sst - 1)){
                        // // console.log('Check this sst 6% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                        addSstTmp = (invoice.amountReceived *100)/106;
                        sstTmp = addSstTmp * 0.06;
                        
                        if(sstTmp >= 0.4){
                        // if(this.totalPenalty >18){
                            // amountList.push((invoice.amountReceived *100)/106)
                            // amountList.push((invoice.amountReceived * 0.06) + invoice.amountRecived)
                            amountList.push(addSstTmp);
                        }else{
                            amountList.push(invoice.amountReceived)
                        }
                                
                    }else{
                        // // console.log('Check this sst 8% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                        
                        addSstTmp = (invoice.amountReceived * 100)/108;
                        sstTmp = addSstTmp * 0.08;
                        
                        if(sstTmp >= 0.4){
                        // if(this.totalPenalty > 18){
                            // amountList.push((invoice.amountReceived *100)/108)
                            // amountList.push((invoice.amountReceived * 0.08) + invoice.amountReceived)
                            amountList.push(addSstTmp);
                        }else{
                            amountList.push(invoice.amountReceived)
                        }                    
                    }

                }

                if(amountList.length > 1){
                    let totalTemp = amountList.reduce((b,a) => b + a,0)
                    console.log(totalTemp,":",this.totalPenalty - totalTemp,"check this")
                    if((this.totalPenalty - totalTemp) < 0){

                        Swal.fire('', 'Collection Receiving Amount can not be more than Total Penalty', 'error');
                        invoice.amountReceived =  0
                        // this.totalAmtcheckWithPenaltyFlag = true;
                    }else{
                        // this.totalAmtcheckWithPenaltyFlag = false;
                    }
                    
                }



                
            }
        })
        // console.log("amountList :",amountList)
        this.sum = amountList.reduce((a, b) => a + b, 0);
        let amt = this.fin09sd.totalPenalty
        // console.log('fin09...',this.fin09sd,"amt : ",amt,"sum : ",this.sum);
        this.sums = amt - this.sum

        // let sstCheck = Math.round((this.fin09sd.invoiceBaseValue * 0.06)*100)/100;
        // let removeSst = 0;
        // if(sstCheck <= (data.sst + 1) &&  sstCheck >= (data.sst - 1)){
        //     // // console.log('Check this sst 6% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
        //     removeSst = (data.paymentReceived *100)/106
                            
        // }else{
        //     // // console.log('Check this sst 8% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
        //     removeSst = (data.paymentReceived *100)/108
                    
        // }

    }

    //this fuction called on keystroke...
    amountFunc(e: any, id: number) {
        // if(this.sums < 0){
        //     Swal.fire('', 'Collection Receiving Amount can not be more than Total Penalty or Retention Amount', 'error');
        // }
        let tempCalc : Number = 0;
        console.log(this.filterDatas,"check this. filter data")
        this.filterDatas.forEach(invoice => {
            if(invoice.amountReceived != ""){
                tempCalc += invoice.amountReceived;
                console.log("tempCalc",tempCalc);
            }
            
            if (invoice.id == id) {
                let value = e.target.value === "" ? "" : e.target.value.replace(/[^(0-9.s)]/gi, '');
                if (parseFloat(value) > this.fin09sd.totalPenalty || parseFloat(value) > invoice.netRetentionAmount ) {
                    Swal.fire('', 'Collection Receiving Amount can not be more than Total Penalty or Retention Amount', 'error');
                }
                // if (parseFloat(value) > this.sums || parseFloat(value) > invoice.netRetentionAmount) {
                //     Swal.fire('', 'Collection Receiving Amount can not be more than Total Penalty or Retention Amount', 'error');
                // }
                // if(this.sums < 0){
                //     Swal.fire("","tada","error")
                // }
                invoice.amountReceived =  (value === "") ? "" : (parseFloat(value) > this.fin09sd.totalPenalty || parseFloat(value) > invoice.netRetentionAmount) ? "" : parseFloat(value);
                
            }

        })

        this.handleTotalAmountPaid();
    }







    //this function called on onChange...
    amountFunc1(e, id: number) {
        // // console.log('Value..',e,id)
        this.filterDatas.forEach(invoice => {
            // if(this.sums < 0){
            //     Swal.fire('', 'Collection Receiving Amount can not be more test than Total Penalty or Retention Amount', 'error');
            // }
            if (invoice.id == id) {
                if (invoice.amountReceived === 0 || invoice.amountReceived === "") {
                    Swal.fire('', 'Collection Receiving Amount can not be Zero or Empty', 'error');
                    invoice.amountReceived = "";
                }
                // else{
                //     // console.log('invoice : ',invoice.amountReceived)
                //     // console.log("invoice : ",invoice)
                // }
            }
        })

        this.handleTotalAmountPaid();
    }


    update() {

        let finGroupsList = [];
        this.finGroups.forEach(data => {
            if (data.amountReceived === "") {
                Swal.fire('', 'Collection Receiving Amount can not be Empty', 'error');
                finGroupsList = [];
                return;
            } else {
                if (this.sums < 0) {
                    Swal.fire('', 'Penalty Amount cannot be Less than zero', 'error');
                }
                else {
                    // console.log("Initial Fin GRoup List..",data);
                    console.log("the Else is working...")
                    let invoicePaymentData = {
                        invoiceNo: data.invoiceNo,
                        paymentReceived: data.amountReceived,
                        fin09InvoiceNo: this.paymentRefNo,
                        paymentRefNo: "",
                        paymentMode: "Credit Note",
                        updatedBy: JSON.parse(localStorage.getItem('currentUser')).id,
                        // for knowing the SST...
                        sst: data.sst,
                        invoiceBaseValue : data.invoiceBaseValue,
                        totalInvoiceValue  : data.totalInvoiceValue
                    }
                    finGroupsList.push(invoicePaymentData);
                    let stringVal = '';
                    // // console.log("jehdsvbjdv",finGroupsList);

                    this.penaltyAdjNowTotal = 0;
                    this.sstTotal = 0;
                    this.penaltyAdjTotal = 0;

                    finGroupsList.forEach((data) => {
                        
                        let sstCheck = Math.round((data.invoiceBaseValue * 0.06)*100)/100;
                        let removeSst = 0;
                        let addSst = 0;
                        let sst =0;

                         // new code hope finalized:
                        //  this is for the 50 to 53.19 thing
                        if(this.totalPenalty < this.rententionTotal){

                            if(sstCheck <= (data.sst + 1) &&  sstCheck >= (data.sst - 1)){
                                // // console.log('Check this sst 6% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                                // removeSst = (data.paymentReceived *100)/106
                                // addSst = ((data.paymentReceived * 0.06) + data.paymentReceived)
                                addSst = data.paymentReceived / 0.943396226;
                                // sst = addSst * 0.06;
                                sst = addSst - (addSst * 0.943396226);
                                
                                console.log("6% assSst and sst : ",addSst,"---",sst);
                                
                            }else{
                                // // console.log('Check this sst 8% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                                // removeSst = (data.paymentReceived *100)/108
                                // addSst = ((data.paymentReceived * 0.08) + data.paymentReceived)
                                addSst = data.paymentReceived / 0.925925926;
                                // sst = addSst * 0.08;
                                sst = addSst - (addSst * 0.925925926);
                                console.log("8% assSst and sst : ",addSst,"---",sst);
                        
                            }

                        }else if(this.totalPenalty > this.rententionTotal){

                            console.log("else if i s working...")

                            if(sstCheck <= (data.sst + 1) &&  sstCheck >= (data.sst - 1)){
                                // // console.log('Check this sst 6% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                                addSst = (data.paymentReceived *100)/106
                                // addSst = ((data.paymentReceived * 0.06) + data.paymentReceived)
                                // addSst = data.paymentReceived / 0.94;
                                sst = data.paymentReceived -addSst;
                                console.log("6% assSst and sst : ",addSst,"---",sst);
                                
                            }else{
                                // // console.log('Check this sst 8% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                                addSst = (data.paymentReceived *100)/108
                                // addSst = ((data.paymentReceived * 0.08) + data.paymentReceived)
                                // addSst = data.paymentReceived / 0.92;
                                sst = data.paymentReceived - addSst;
                                console.log("8% assSst and sst : ",addSst,"---",sst);
                        
                            }

                        }


                        
                        // removeSst = Math.round((removeSst * 100))/100;
                        //for push purpose
                        // // console.log("SSTCHECK!! : ",data);
                        let penaltyAdjusted = data.paymentReceived;
                        // sst = addSst - (addSst * 0.9433962264150943);
                        // let sst = addSst * 0.9433962264150943;
                        addSst = Math.round((addSst * 100))/100;
                        sst = Math.round((sst *100))/100;
                        // console.log(sst)
                        // if(sst > 1){
                        //     data.paymentReceived = removeSst;
                        //     // removeSst = 0;
                        // }else{
                        //     data.paymentReceived = data.paymentReceived;
                        //     // removeSst = 0;
                        // }   
                        // if(this.totalPenalty >18){
                        if(sst >=0.4 ){

                            // this.penaltyAdjNowTotal += removeSst;
                            // this.sstTotal += sst;
                            // this.penaltyAdjTotal += removeSst;
                            this.penaltyAdjNowTotal += addSst;
                            this.sstTotal += sst;
                            this.penaltyAdjTotal += data.paymentReceived;
                        }else{

                            // this.penaltyAdjNowTotal += data.paymentReceived ;
                            // this.sstTotal += 0;
                            // this.penaltyAdjTotal += data.paymentReceived;
                            this.penaltyAdjNowTotal += data.paymentReceived ;
                            this.sstTotal += 0;
                            this.penaltyAdjTotal += data.paymentReceived;

                        }

                       

                        console.log("before String val : ",addSst,sst);

                        // stringVal += `<tr>
                        //                   <td style=" border: 1px solid black">${data.invoiceNo
                        //   }</td>
                        //   <td style=" border: 1px solid black">${this.totalPenalty > 18?removeSst:data.paymentReceived}</td>
                        //   <td style=" border: 1px solid black">${this.totalPenalty > 18? sst:0}</td>
                        //                   <td style=" border: 1px solid black">${this.totalPenalty > 18?removeSst:data.paymentReceived.toLocaleString(
                        //     "en-US"
                        //   )}</td> 
                          
                        //   </tr>`;
                        // console.log('before swal conformation before String val',data.paymentReceived,"this is SST",addSst);

                        stringVal += `<tr>
                          <td style=" border: 1px solid black">${data.invoiceNo
                            }</td>
                            <td style=" border: 1px solid black">${data.paymentReceived}</td>
                            <td style=" border: 1px solid black">${sst >= 0.4 ? sst:0}</td>
                            <td style=" border: 1px solid black">${sst >= 0.4?addSst:data.paymentReceived.toLocaleString(
                            "en-US"
                            )}</td> 
          
          </tr>`;

                        removeSst = 0;
                        
                        
                    });
    //                 let textFireCollectionDate =
    //   `<h6>Collection Date : ` +
    //   this.datepipe.transform(this.selectedDate, "dd/MM/yyyy") +
    //   `</h6><br>`;
    // let textFireTotalCollection = `<p style="text-align: left;margin-top: 5px;font-size: 17px;"><b>Total Collection = ${this.totalCollection.toLocaleString(
    //   "en-US"
    // )}</b></p>`;
    // // console.log(stringVal)

    Swal.fire({
      title: "Are you sure you want to update?",
      html:
        `<table class="float-center" style="table-layout: fixed; width: 100%; border: 1px solid black">
                        <tr><th style=" border: 1px solid black">INV NO</th><th style=" border: 1px solid black">Penalty Adjusted</th><th style=" border: 1px solid black">SST</th><th style=" border: 1px solid black">Penalty Adjusted Now</th></tr>
            ${stringVal}
            <tr><td style=" border: 1px solid black"><strong>Total</strong></td><td>${this.penaltyAdjTotal.toFixed(2)}</td><td>${this.sstTotal.toFixed(2)}</td><td>${this.penaltyAdjNowTotal.toFixed(2)}</td></td></table>`,
        // textFireTotalCollection,
      type: "warning",
      showCloseButton: true,
      showCancelButton: true,
    }).then((willDelete) => {
      if (willDelete.dismiss) {
        // // console.log('if worked')
        Swal.fire("", "Transaction Cancelled", "error");
        stringVal = "";
        
        // console.log('finList .....',finGroupsList);
      } else {
        // // console.log('else worked')

        finGroupsList.forEach(data =>{

            let sstCheck = Math.round((data.invoiceBaseValue * 0.06)*100)/100;
            let removeSst = 0;
            let addSst = 0;
            let sst = 0;

            // console.log("payment recived before all the stuffs starting",data.paymentReceived);
            if(this.totalPenalty < this.rententionTotal){
                if(sstCheck <= (data.sst + 1) &&  sstCheck >= (data.sst - 1)){
                    // // console.log('Check this sst 6% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                    // removeSst = (data.paymentReceived *100)/106
                    // addSst = data.paymentReceived + (data.paymentReceived * 0.06)
                    addSst = data.paymentReceived /0.943396226;
                    // sst = addSst * 0.06
                    sst = addSst - (addSst * 0.943396226)
                    // console.log('sstCheck and setting the addSst 6 : ',addSst)
                                
                }else{
                    // // console.log('Check this sst 8% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                    // removeSst = (data.paymentReceived *100)/108
                    // addSst = data.paymentReceived + (data.paymentReceived * 0.08);
                    addSst = data.paymentReceived / 0.925925926;
                    // sst = addSst * 0.08
                    sst = addSst - (addSst * 0.925925926)
                    // console.log('sstCheck and setting the addSst 8 : ',addSst)
    
                }
            
            }else if(this.totalPenalty > this.rententionTotal){

                console.log("else if i s working...")

                if(sstCheck <= (data.sst + 1) &&  sstCheck >= (data.sst - 1)){
                    // // console.log('Check this sst 6% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                    // removeSst = (data.paymentReceived *100)/106
                    // addSst = data.paymentReceived + (data.paymentReceived * 0.06)
                    addSst = (data.paymentReceived *100)/106;
                    sst = data.paymentReceived - addSst;
                    // console.log('sstCheck and setting the addSst 6 : ',addSst)
                                
                }else{
                    // // console.log('Check this sst 8% !!!!!!!!!!!!!!!!!',sstCheck, data.sst)
                    // removeSst = (data.paymentReceived *100)/108
                    // addSst = data.paymentReceived + (data.paymentReceived * 0.08);
                    addSst = (data.paymentReceived *100)/108;
                    sst = data.paymentReceived - addSst;
                    // console.log('sstCheck and setting the addSst 8 : ',addSst)
    
                }
            }
            
            // removeSst = Math.round((removeSst * 100))/100;
            //for push purpose
            // // console.log("SSTCHECK!! : ",data);
            let penaltyAdjusted = data.paymentReceived;
            // let sst = data.paymentReceived - removeSst;
            // console.log("payment recived new ...",data.paymentReceived,"this is the Add sst",addSst);
            // console.log("Add sst new working ...",addSst);
            // let sst = addSst - (addSst * 0.9433962264150943);
            // let sst = addSst  
            addSst = Math.round((addSst * 100))/100;
            sst = Math.round((sst *100))/100;
            // console.log("sst new working......",sst)
            if(sst >= 0.4){
                // console.log("if working")
            // if(this.sums > 18){
                // console.log("payment reciving data in if",data.paymentReceived)
                data.paymentReceived = addSst;
                // removeSst = 0;
            }else{
                // console.log("else working")
                // console.log("payment reciving data in else",data.paymentReceived)
                data.paymentReceived = data.paymentReceived;
                // removeSst = 0;
            }
            // console.log("===============================================================")
            // console.log("paymentRecived : ",data.paymentReceived);
            // console.log("===============================================================")

        })
        console.log("finGroupList : ",finGroupsList);
        console.log("Total PEnalty : ",this.totalPenalty)
        this.totalPenalty = this.totalPenalty - (Math.round((this.totalPenalty * 0.06) * 100) /100) 
        
        this.paiService.penaltyInvoicePayment(finGroupsList).subscribe(x => {
                        Swal.fire('', 'Penalty Adjustment Successfully. Transaction Reference No - ' + x[0].transactionRefNo, 'success');
                        //   this.router.navigateByUrl('transaction/credit-note/fin09/created-list')
                        history.back();
                    })
        this.clicked = true;
        this.ngOnInit();
      }
    });

                    // Swal.fire({
                    //     title: 'Are you sure?',
                    //     text: 'To Proceed Penalty Adjustment for Invoice',
                    //     type: 'warning',
                    //     showCloseButton: true,
                    //     showCancelButton: true
                    // }).then((willDelete) => {
                    //     if (willDelete.dismiss) {
                    //         Swal.fire('', 'Penalty Adjustment Not Successful', 'error');
                    //         history.back();
                    //     } else {

                    //         this.paiService.penaltyInvoicePayment(finGroupsList).subscribe(x => {
                    //             Swal.fire('', 'Penalty Adjustment Successfully. Transaction Reference No - ' + x[0].transactionRefNo, 'success');
                    //             //   this.router.navigateByUrl('transaction/credit-note/fin09/created-list')
                    //             history.back();
                    //         })
                    //     }
                    // });
                }



            }
        });
    }


    filterFunction() {
        this.finGroupsList = [];
        this.finGroups = [];
        this.clicked = false
        this.loading = true;
        this.filterDatas = [];
        // this.month = ''
        this.storings = []


        // if (this.approvalQuater == 'Q4') {
        //     this.month = 11;

        // }
        // else if (this.approvalQuater == 'Q3') {
        //     this.month = 8;

        // }
        // else if (this.approvalQuater == 'Q2') {
        //     this.month = 5;

        // }
        // else if (this.approvalQuater == 'Q1') {
        //     this.month = 2;

        // }



        this.paiService.fetchAllInvoiceFilter1(this.stateFilter, this.districtFilter, this.clinicTypeCode).subscribe((x) => {

            // // console.log("Before the filter is applied to Filter Data..",x)

            this.dtTrigger.next();

            let date_1 = new Date(this.approvalYear, this.month - 1);


            x.forEach((element, i) => {
                element.intMonthAndYear = new Date(parseInt(element.year), parseInt(element.month) - 1)
                this.storings.push(element)
                if (x.length - 1 == i) {

                    this.storings.forEach((element, i) => {
                        if ((element.invoiceTypeId == '3' || element.invoiceTypeId == '4') && (element.intMonthAndYear <= date_1)) {
                            this.filterDatas.push(element)
                            // // console.log("intMonthYear : ",element.intMonthAndYear);
                            // // console.log("date_1 : ",date_1);
                            if (this.storings.length - 1 == i) {

                            }
                        }
                    });
                }


                // if (x.length - 1 == i) {

                //     this.storings.forEach((element, i) => {
                //         if ((element.invoiceTypeId == '3' || element.invoiceTypeId == '4' && (element.intMonthAndYear <= date_1))) {
                //             this.filterDatas.push(element)
                //             if (this.storings.length - 1 == i) {

                //             }
                //         }
                //     });
                // }
            });
            // // console.log("Storings..",this.storings);

            // // console.log("This is the filter Data..",this.filterDatas);




            if (this.filterDatas && this.filterDatas.length > 0) {
                this.filterDatas.forEach(invoice => {
                    invoice.enableListData = false;
                    invoice.disableCollectionAmount = true;
                    invoice.disableCollectionRefNo = true;
                    invoice.amountReceived = "";
                    invoice.amountRefNo = "";
                })

            }

            console.log(this.filterDatas)
            this.rententionTotal = this.filterDatas.reduce((a,x) => a + x.retentionAmount,0)
            this.rententionTotal = Math.round(this.rententionTotal * 100)/100;

            console.log(this.rententionTotal,"this is the totalRetention")

        });
        this.loading = false


    }
}