import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

@Injectable()
export class SpecialApprovalService{

    serviceApiUrl: string = environment.serviceApiUrl;
    constructor(private http: HttpClient) { }
    

    public fetchRentalList(stateId,districtId,clinicTypeId,fromMonth,toMonth,fromYear,toYear){
        return this.http.get<Array<any>>(this.serviceApiUrl + 'rentalEquipmentInfo/fetchData?stateId='+stateId+'&districtId='+districtId+'&clinicTypeId='+clinicTypeId+'&fromMonth='+fromMonth+'&toMonth='+toMonth+'&fromYear='+fromYear+'&toYear='+toYear)
    }

    public fetchRentalListGroupByState(month,year){
        return this.http.get<Array<any>>(this.serviceApiUrl + 'rentalEquipmentInfo/fetchDataGroupByState?month='+month+'&year='+year)
    }

    public fetchRentalListGroupByDistrict(stateId,month,year){
        return this.http.get<Array<any>>(this.serviceApiUrl + 'rentalEquipmentInfo/fetchDataGroupByDistrict?stateId='+stateId+'&month='+month+'&year='+year)
    }

    public fetchRentalListGroupByBeNumberAndClinicTypeId(districtId,month,year){
        return this.http.get<Array<any>>(this.serviceApiUrl + 'rentalEquipmentInfo/fetchDataGroupByClinicAndBeNumber?districtId='+districtId+'&month='+month+'&year='+year)
    }
}