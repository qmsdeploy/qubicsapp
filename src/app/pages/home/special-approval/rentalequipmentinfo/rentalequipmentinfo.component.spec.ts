import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentalequipmentinfoComponent } from './rentalequipmentinfo.component';

describe('RentalequipmentinfoComponent', () => {
  let component: RentalequipmentinfoComponent;
  let fixture: ComponentFixture<RentalequipmentinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentalequipmentinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentalequipmentinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
