import { Component, OnInit, ViewChild } from '@angular/core';
//import { ReportService } from "../../report.service";
//import { Invoice } from "../../model/invoice";
import { DataTableDirective } from 'angular-datatables';
//import { InvoiceType } from "../../model/invoice-type";
import { Subject } from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as dateFormat from 'dateformat';
import pdfMake from "pdfmake/build/pdfmake.min";
import pdfFonts from "pdfmake/build/vfs_fonts";

import { formatDate, formatNumber } from '@angular/common';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { SpecialApprovalService } from '../specialApprovalService';
import { MasterService } from '../../master/master.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-rentalequipmentinfo',
  templateUrl: './rentalequipmentinfo.component.html',
  styleUrls: ['./rentalequipmentinfo.component.scss']
})
export class RentalequipmentinfoComponent implements OnInit {

  
  dtOptions: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength: 10
  };
  dtOptions1: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength: 10
  };

  dtOptions2: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength: 10
  };

  dtOptions3: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength: 10
  };

  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  dtTrigger1: Subject<any> = new Subject();
  @ViewChild(DataTableDirective, { static: false })
  dtElement1: DataTableDirective;

  dtTrigger2: Subject<any> = new Subject();
  @ViewChild(DataTableDirective, { static: false })
  dtElement2: DataTableDirective;

  dtTrigger3: Subject<any> = new Subject();
  @ViewChild(DataTableDirective, { static: false })
  dtElement3: DataTableDirective;

  public stateFilterDatas : any;
  public stateFilter : string;
  public stateFilterId  : Number;
  public districtFilterDatas  :any;
  public districtFilter : any;
  public districtFilterId : Number;
  public clinicTypeFilter : any;
  public loading : boolean;
  public rentalList : any;
  public rentalInfogroupByState : any;
  public rentalInfogroupByDistrict : any;  
  public rentalInfogroupByBeNumber : any;
  public fromMonth : Number;
  public toMonth : Number;
  public fromYear : Number;
  public toYear : Number;
  public selectedMonth : String;
  public selectedYear :  Number;
  public selectedState : String;
  public selectedDistrict : String;
  public districtStyle : boolean;
  public stateStyle : boolean;
  public rentalInfogroupByTotal : Number;
  public rentalInfogroupByStateTotal : Number;
  public rentalInfogroupByDistrictTotal : Number;
  public rentalInfogroupByBeNumberTotal : Number;

  public monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];

  constructor(private specialApprovalService : SpecialApprovalService,private masterService : MasterService,private modalService : NgbModal){}


  ngOnInit(){

    this.rentalList = [];
    this.rentalInfogroupByState = [];
    this.rentalInfogroupByDistrict = [];
    this.rentalInfogroupByBeNumber = [];

    this.rentalInfogroupByTotal = 0;
    this.rentalInfogroupByStateTotal = 0;
    this.rentalInfogroupByDistrictTotal = 0;
    this.rentalInfogroupByBeNumberTotal = 0;
    
    // this.dtOptions  = {
    //   pagingType : 'full_numbers',
    //   pageLength : 10
    // };

    this.masterService.getAllState().subscribe(state =>{
      this.stateFilterDatas = [];
      this.stateFilterDatas = state;
    })

    this.masterService.getAllDistrict().subscribe(district => {
      this.districtFilterDatas = [];
      this.districtFilterDatas = district;
    })

    

  }

  handleForm(event) {
    switch (event.target.id) {
      case "state":
        this.stateFilter = event.target.value;
        this.districtFilter = "";
        this.districtFilterDatas = this.stateFilterDatas.find(state => state.stateName == event.target.value).districts
        this.stateFilterId = this.stateFilterDatas.find(stateId => stateId.stateName == event.target.value).id;
        console.log('stateFilterDataId : ',this.stateFilterId);
        break;
      case "district":
        this.districtFilter = event.target.value;
        if(this.districtFilter == ''){
          this.districtFilterId = null;
        }else{
          this.districtFilterId = this.districtFilterDatas.find(districtId => districtId.districtName == event.target.value).id;
        }
        console.log("districtFilterDataId : ",this.districtFilterId);
        break;
      case "clinicType":
        this.clinicTypeFilter = event.target.value;
        break;
      
      default:
        break;
    }

    console.log("Filter Datas :",this.stateFilterId,this.districtFilter,this.clinicTypeFilter);
  }

  getData(){

    this.specialApprovalService.fetchRentalList(this.stateFilterId == null ? '' : this.stateFilterId,this.districtFilterId == null ? '' : this.districtFilterId,this.clinicTypeFilter == null ? '' : this.clinicTypeFilter,'','',this.fromYear == null? '' : this.fromYear,this.toYear == null? '': this.toYear ).subscribe(x => {
      console.log('X',x)
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.rentalList = x;
      this.dtTrigger.next();
      })
      this.rentalInfogroupByTotal = x.reduce((a,b) => Number(a) + Number(b.totalRentalCharges),0)
    })

  }

  openModal(template,data:any){

    switch(template._declarationTContainer.localNames[0]){

      case 'groupByState' :
        this.selectedYear = data.year;
        this.selectedMonth = this.monthNames[data.month - 1];
        this.specialApprovalService.fetchRentalListGroupByState(data.month,data.year).subscribe(x =>{
          this.dtElement1.dtInstance.then((dtInstance: DataTables.Api) => {
          // dtInstance.destroy();
          this.rentalInfogroupByState = []
          this.rentalInfogroupByState = x;
          this.dtTrigger1.next();
          })
          this.rentalInfogroupByStateTotal = x.reduce((a,b) => Number(a) + Number(b.totalRentalCharges),0)


        })
      break;

      case 'groupByDistrict' :
        this.selectedState =data.stateName;
        this.specialApprovalService.fetchRentalListGroupByDistrict(data.stateId,data.month,data.year).subscribe(y =>{
          this.dtElement2.dtInstance.then((dtInstance: DataTables.Api) => {
          // dtInstance.destroy();
          this.rentalInfogroupByDistrict = [];
          this.rentalInfogroupByDistrict = y;
          this.dtTrigger2.next();
          })
          this.rentalInfogroupByDistrictTotal = y.reduce((a,b) => Number(a) + Number(b.rentalCharges),0)
        })        
      break;

      case 'groupByBeNumber' :
        this.selectedDistrict = data.districtName;
        this.specialApprovalService.fetchRentalListGroupByBeNumberAndClinicTypeId(data.districtId,data.month,data.year).subscribe(z =>{
          this.dtElement3.dtInstance.then((dtInstance: DataTables.Api) => {
          // dtInstance.destroy();
          this.rentalInfogroupByBeNumber = [];
          this.rentalInfogroupByBeNumber = z;
          this.dtTrigger3.next();
          })
          this.rentalInfogroupByBeNumberTotal = z.reduce((a,b) => Number(a) + Number(b.rentalCharges),0)
        })
      break;
      
    }
    this.modalService.open(template,{
      size : 'lg',centered : true
    });

  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
    // console.log('BByyyeeeeeeeeeeeee');
  }



  downloadPdf(){
    // console.log('PDF');
    // let logo = this.Datas.logo;

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    
    const document ={        
      pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
        //   columns:[
        //   {
        //     image: logo,
        //     fit: [275, 125],
        //     alignment: 'center',
        //     margin: [0, 0, 0, 10],
            
        //   },
        // ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
        {
          text: '\nRental Equipment List' + '\n',
          style: 'header'
        },
  
        {
          text: this.stateFilter + ' State/ ' + this.districtFilter +' District/ ' + this.clinicTypeFilter +'\n',
          style: 'subHeader'
        },
  
        {
          style: 'table',
  
          table: {
            widths:['*','*','*','*'],
            body: [
              [ 
                // {text : 'Sno' , style : 'tableHead'},
              {text : 'Year' , style : 'tableHead'},
              {text : 'Month' , style : 'tableHead'},
              {text : 'Rental Revenue (RM)' , style : 'tableHead'}],
              ...this.rentalList.map(x =>([x.year,this.monthNames[x.month - 1],x.rentalCharges.toFixed(2)])),
            ]
          },
          alignment : 'left',
        },
        
  
        ],

      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'center'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }

    }
    pdfMake.createPdf(document).download("Rental Equipment List.pdf");

  }

  generateExcel(){

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    // console.log('xcel start..');
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('RentalEquipmentList');
    let data = [];
    //console.log(data);
  
    const title = worksheet.addRow(['', '', 'Rental Equipment List','']);
    title.font={
      size: 12,
      bold:true
    }
    worksheet.mergeCells('D1:H1');
    worksheet.addRow([]);
    worksheet.addRow([]);
    
    worksheet.addRow(["STATE :" + this.stateFilter,'','DISTRICT :'+this.districtFilter,'']);
    worksheet.mergeCells('A4:B4');
    worksheet.addRow(['CLINIC TYPE :'+this.clinicTypeFilter,'']);
    worksheet.mergeCells('A5:B5');
    
    // worksheet.addRow(['YEAR :'+this.Datas.dataYear]);
    worksheet.addRow([]);
    worksheet.addRow([]);
  
    let header = worksheet.addRow(['#','Year','Month','Rental Revenue (RM)']);
    header.eachCell(cell => {
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    
    header.font={
      size : 12,
      bold:true,
    }
    
    let i =1;
    for(let d of this.rentalList){
      //data=[];
      data.push([i,d.year,this.monthNames[d.month - 1],d.rentalCharges.toFixed(2)]);
      i++;
    }
    for(let d of data){
      let row = worksheet.addRow(d);
      row.eachCell(cell => {
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
    }
  
    worksheet.getColumn(2).width = 18;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 50;
  
    //footer:
    let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
  
    let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
      footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      //Merge Cells
      worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
      worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
      worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
  
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Rental Equipment List.xlsx');
    })
  
  }

  downloadPdfGroupByState(){
    // console.log('PDF');
    // let logo = this.Datas.logo;

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    
    const document ={        
      pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
        //   columns:[
        //   {
        //     image: logo,
        //     fit: [275, 125],
        //     alignment: 'center',
        //     margin: [0, 0, 0, 10],
            
        //   },
        // ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
        {
          text: '\nRental Equipment List' + '\n',
          style: 'header'
        },
  
        {
          text: this.stateFilter + ' State/ ' + this.districtFilter +' District/ ' + this.clinicTypeFilter +'\n',
          style: 'subHeader'
        },
  
        {
          style: 'table',
  
          table: {
            widths:['*','*','*','*','*'],
            body: [
              [ 
                // {text : 'Sno' , style : 'tableHead'},
              {text : 'Year' , style : 'tableHead'},
              {text : 'Month' , style : 'tableHead'},
              {text : 'State' , style : 'tableHead'},
              {text : 'Rental Revenue (RM)' , style : 'tableHead'}],
              ...this.rentalInfogroupByState.map(x =>([x.year,this.monthNames[x.month - 1],x.stateName,x.rentalCharges.toFixed(2)])),
            ]
          },
          alignment : 'left',
        },
        
  
        ],

      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'center'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }

    }
    pdfMake.createPdf(document).download("Rental Equipment List.pdf");

  }

  generateExcelGroupByState(){

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    // console.log('xcel start..');
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('RentalEquipmentList');
    let data = [];
    //console.log(data);
  
    const title = worksheet.addRow(['', '', 'Rental Equipment List','']);
    title.font={
      size: 12,
      bold:true
    }
    worksheet.mergeCells('D1:H1');
    worksheet.addRow([]);
    worksheet.addRow([]);
    
    worksheet.addRow(["STATE :" + this.stateFilter,'','DISTRICT :'+this.districtFilter,'']);
    worksheet.mergeCells('A4:B4');
    worksheet.addRow(['CLINIC TYPE :'+this.clinicTypeFilter,'']);
    worksheet.mergeCells('A5:B5');
    
    // worksheet.addRow(['YEAR :'+this.Datas.dataYear]);
    worksheet.addRow([]);
    worksheet.addRow([]);
  
    let header = worksheet.addRow(['#','Year','Month','State','Rental Revenue (RM)']);
    header.eachCell(cell => {
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    
    header.font={
      size : 12,
      bold:true,
    }
    
    let i =1;
    // console.log("adlkfndsjnvos",this.re)
    for(let d of this.rentalInfogroupByState){
      //data=[];
      data.push([i,d.year,this.monthNames[d.month - 1],d.stateName,d.rentalCharges.toFixed(2)]);
      i++;
    }
    for(let d of data){
      let row = worksheet.addRow(d);
      row.eachCell(cell => {
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
    }
  
    worksheet.getColumn(2).width = 18;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 50;
    worksheet.getColumn(5).width = 50;
  
    //footer:
    let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
  
    let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
      footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      //Merge Cells
      worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
      worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
      worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
  
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Rental Equipment List.xlsx');
    })
  
  }

  downloadPdfGroupByDistrict(){
    // console.log('PDF');
    // let logo = this.Datas.logo;

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    
    const document ={        
      pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
        //   columns:[
        //   {
        //     image: logo,
        //     fit: [275, 125],
        //     alignment: 'center',
        //     margin: [0, 0, 0, 10],
            
        //   },
        // ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
        {
          text: '\nRental Equipment List' + '\n',
          style: 'header'
        },
  
        {
          text: this.stateFilter + ' State/ ' + this.districtFilter +' District/ ' + this.clinicTypeFilter +'\n',
          style: 'subHeader'
        },
  
        {
          style: 'table',
  
          table: {
            widths:['*','*','*','*','*'],
            body: [
              [ 
                // {text : 'Sno' , style : 'tableHead'},
              // {text : 'Year' , style : 'tableHead'},
              {text : 'State' , style : 'tableHead'},
              {text : 'District' , style : 'tableHead'},
              {text : 'Rental Revenue (RM)' , style : 'tableHead'}],
              ...this.rentalInfogroupByDistrict.map(x =>([x.stateName,x.districtName,x.rentalCharges.toFixed(2)])),
            ]
          },
          alignment : 'left',
        },
        
  
        ],

      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'center'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }

    }
    pdfMake.createPdf(document).download("Rental Equipment List.pdf");

  }

  generateExcelGroupByDistrict(){

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    // console.log('xcel start..');
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('RentalEquipmentList');
    let data = [];
    //console.log(data);
  
    const title = worksheet.addRow(['', '', 'Rental Equipment List','']);
    title.font={
      size: 12,
      bold:true
    }
    worksheet.mergeCells('D1:H1');
    worksheet.addRow([]);
    worksheet.addRow([]);
    
    worksheet.addRow(["STATE :" + this.stateFilter,'','DISTRICT :'+this.districtFilter,'']);
    worksheet.mergeCells('A4:B4');
    worksheet.addRow(['CLINIC TYPE :'+this.clinicTypeFilter,'']);
    worksheet.mergeCells('A5:B5');
    
    // worksheet.addRow(['YEAR :'+this.Datas.dataYear]);
    worksheet.addRow([]);
    worksheet.addRow([]);
  
    let header = worksheet.addRow(['#','State','District','Rental Revenue (RM)']);
    header.eachCell(cell => {
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    
    header.font={
      size : 12,
      bold:true,
    }
    
    let i =1;
    for(let d of this.rentalInfogroupByDistrict){
      //data=[];
      data.push([i,d.stateName,d.districtName,d.rentalCharges.toFixed(2)]);
      i++;
    }
    for(let d of data){
      let row = worksheet.addRow(d);
      row.eachCell(cell => {
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
    }
  
    worksheet.getColumn(2).width = 18;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 50;
    // worksheet.getColumn(5).width = 50;
  
    //footer:
    let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
  
    let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
      footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      //Merge Cells
      worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
      worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
      worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
  
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Rental Equipment List.xlsx');
    })
  
  }

  downloadPdfGroupByClinic(){
    // console.log('PDF');
    // let logo = this.Datas.logo;

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    
    const document ={        
      pageMargins: [30, 135, 30, 60],
      header: function () {
        return{
        //   columns:[
        //   {
        //     image: logo,
        //     fit: [275, 125],
        //     alignment: 'center',
        //     margin: [0, 0, 0, 10],
            
        //   },
        // ]     
      }
      },

      footer: function (currentPage, pageCount) {
        let footerText = [
          {
            columns: [
              {
                stack: [
                  {
                    text: 'Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'),
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [50, 0, 0, 0]
              },
              {
                stack: [
                  {
                    text: 'Username :' + JSON.parse(localStorage.getItem('currentUser')).name,
                    bold: true,
                    fontSize: 10,
                    // margin: [0, 0, 0, 10]
                  }
                ], margin: [30, 0, 0, 0]
              },
              {
                stack: [
                  { text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 10, margin: [0, 0, 40, 0] }
                ], margin: [0, 0, 30, 0]
              }
            ],
          }
        ]
        return footerText;
      },

      content: [
        {
          text: '\nRental Equipment List' + '\n',
          style: 'header'
        },
  
        {
          text: this.stateFilter + ' State/ ' + this.districtFilter +' District/ ' + this.clinicTypeFilter +'\n',
          style: 'subHeader'
        },
  
        {
          style: 'table',
  
          table: {
            widths:['*','*','*','*','*'],
            body: [
              [ 
                {text : 'Clinic Code' , style : 'tableHead'},
              {text : 'Clinic Name' , style : 'tableHead'},
              {text : 'Clinic Type' , style : 'tableHead'},
              {text : 'BE Number' , style : 'tableHead'},
              {text : 'Rental Revenue (RM)' , style : 'tableHead'}],
              ...this.rentalInfogroupByBeNumber.map(x =>([x.clinicCode,x.clinicName,x.clinicTypeCode,x.beNumber,x.rentalCharges.toFixed(2)])),
            ]
          },
          alignment : 'left',
        },
        
  
        ],

      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment : 'center'
        },
        tableHead:{
          bold : true,
          alignment : 'center'
        },
        subHeader:{
          fontSize:12,
          bold: true,
          alignment : 'center'
        },
      }

    }
    pdfMake.createPdf(document).download("Rental Equipment List.pdf");

  }

  generateExcelGroupByClinic(){

    this.stateFilter = this.stateFilter == null ?'ALL':this.stateFilter;
    this.districtFilter = this.districtFilter == null?'ALL':this.districtFilter;
    this.clinicTypeFilter = this.clinicTypeFilter == null?'ALL':this.clinicTypeFilter;
    //
    // console.log('xcel start..');
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('RentalEquipmentList');
    let data = [];
    //console.log(data);
  
    const title = worksheet.addRow(['', '', 'Rental Equipment List','']);
    title.font={
      size: 12,
      bold:true
    }
    worksheet.mergeCells('D1:H1');
    worksheet.addRow([]);
    worksheet.addRow([]);
    
    worksheet.addRow(["STATE :" + this.stateFilter,'','DISTRICT :'+this.districtFilter,'']);
    worksheet.mergeCells('A4:B4');
    worksheet.addRow(['CLINIC TYPE :'+this.clinicTypeFilter,'']);
    worksheet.mergeCells('A5:B5');
    
    // worksheet.addRow(['YEAR :'+this.Datas.dataYear]);
    worksheet.addRow([]);
    worksheet.addRow([]);
  
    let header = worksheet.addRow(['#','Clinic Code','Clinic Name','Clinic Type','BE Number','Rental Revenue (RM)']);
    header.eachCell(cell => {
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    
    header.font={
      size : 12,
      bold:true,
    }
    
    let i =1;
    for(let d of this.rentalInfogroupByBeNumber){
      //data=[];
      data.push([i,d.clinicCode,d.clinicName,d.clinicTypeCode,d.beNumber,d.rentalCharges.toFixed(2)]);
      i++;
    }
    for(let d of data){
      let row = worksheet.addRow(d);
      row.eachCell(cell => {
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
    }
  
    worksheet.getColumn(2).width = 18;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 50;
    worksheet.getColumn(5).width = 50;
    // worksheet.getColumn(5).width = 50;
  
    //footer:
    let footerRow = worksheet.addRow(['This is system generated excel sheet.']);
  
    let footerDate = worksheet.addRow(['Date : ' + dateFormat(new Date(), 'dd-mm-yyyy hh:mm:ss TT'), '', '', 'User Name : ' + JSON.parse(localStorage.getItem('currentUser')).name]);
      footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      //Merge Cells
      worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
      worksheet.mergeCells(`A${footerDate.number}:B${footerDate.number}`);
      worksheet.mergeCells(`D${footerDate.number}:F${footerDate.number}`);
  
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Rental Equipment List.xlsx');
    })
  
  }


}


 


