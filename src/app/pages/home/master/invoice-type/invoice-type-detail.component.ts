import { Component, OnInit } from '@angular/core';
import { MasterService } from "../master.service";
import 'rxjs/add/operator/switchMap';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { InvoiceType } from '../model/invoice-type';


@Component({
    selector: 'invoice-type-detail',
    templateUrl: './invoice-type-detail.component.html'
})
export class InvoiceTypeDetailComponent implements OnInit {
    public invoiceType: InvoiceType;
    public retentionDisable: Boolean = false;
    public sstDisable: Boolean = false;
    public loading: Boolean = true;
    constructor(public masterService: MasterService, public router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.invoiceType = new InvoiceType();
        this.retentionDisable = this.invoiceType.retentionAvailable == 'N' ? true : false
        this.sstDisable = this.invoiceType.sstIncluded == 'N' ? true : false
        if (this.route.params['_value']['_id'] != "undefined") {
            this.route.params.switchMap((par: Params) => this.masterService.getInvoiceTypeById(par['_id'])).subscribe(x => {
                this.invoiceType = x;
                this.retentionDisable = this.invoiceType.retentionAvailable == 'N' ? true : false
                this.sstDisable = this.invoiceType.sstIncluded == 'N' ? true : false
                this.loading = false;
            });
        }
        this.loading = false;

    }

    handleRetention() {
        this.retentionDisable = this.invoiceType.retentionAvailable == 'N' ? true : false
        this.invoiceType.retentionPercentage = this.invoiceType.retentionAvailable == 'N' ? 0 : this.invoiceType.retentionPercentage
    }

    handleSst() {
        this.sstDisable = this.invoiceType.sstIncluded == 'N' ? true : false
        this.invoiceType.sstPercentage = this.invoiceType.sstIncluded == 'N' ? 0 : this.invoiceType.sstPercentage
    }


    save() {

        // console.log('Name',this.invoiceType.invoiceTypeName,'Code',this.invoiceType.invoiceTypeCode)

        if (this.invoiceType.invoiceTypeName == null || this.invoiceType.invoiceTypeCode == null || this.invoiceType.invoiceTypeCode == '' || this.invoiceType.invoiceTypeName == '') {
            Swal.fire('', 'Please Fill all the Fields', 'error')
        } else {

            let invoiceTypes: Array<any>;

            this.masterService.getAllInvoiceTypes().subscribe(x => {
                invoiceTypes = x.filter(obj => obj.id != this.invoiceType.id);
                // console.log(invoiceTypes)

                if (invoiceTypes.find(x => x.invoiceTypeName === this.invoiceType.invoiceTypeName)) {
                    // console.log("Already exists")
                    Swal.fire('', 'The Invoice Type Name Already Exists', 'error')
                } else if (invoiceTypes.find(x => x.invoiceTypeCode === this.invoiceType.invoiceTypeCode)) {
                    Swal.fire('', 'The Invoice Type Code Already Exists', 'error')
                } else {
                    // console.log("New Stuff")
                    if (this.invoiceType.id != undefined) {
                        this.masterService.updateInvoiceType(this.invoiceType).subscribe((x) => {
                            Swal.fire('', 'Invoice Type updated successfully!!!', 'success');
                            this.exit();
                        });
                    } else {
                        this.masterService.addInvoiceType(this.invoiceType).subscribe((x) => {
                            Swal.fire('', 'Invoice Type added successfully!!!', 'success');
                            this.exit();
                        });
                    }
                }
            });

        }


    }


    exit() {
        this.router.navigateByUrl("/master/invoice-type-list");
    }

}
