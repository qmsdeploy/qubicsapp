export class User {
    public id: number
    public jwt: string;
    public userName: string;
    public email: string;
    public roles: Array<string>;
}