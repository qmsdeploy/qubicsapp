import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthLoginComponent } from './auth-login.component';
import { AuthResetPasswordV2Component } from './auth-reset-password-v2.component';
import { AuthChangePasswordV2Component } from './auth-change-password-v2.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'login', component: AuthLoginComponent },
      { path: 'reset-password', component: AuthResetPasswordV2Component },
      //{ path: 'reset-password/:_key', component: AuthResetPasswordV2Component },
      { path: 'change-password', component: AuthChangePasswordV2Component }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
